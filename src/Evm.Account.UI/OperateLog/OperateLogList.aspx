﻿<%@ Page Language="C#" AutoEventWireup="true"  ClientIDMode="Static"  EnableViewState="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <am:Style runat="server" />
    <script type="text/javascript">
    using(['dialog',"messager"])
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <div id="tt"></div>
    <div id='searchBox' buttons="#dlg-buttons" style="display:none;padding:5px 10px">
        <div style="padding:10px 5px">
            <p ><label>姓名：</label>
                <input id="userName" type="text"  />
            </p>
            <p>
                <label>关键字：</label>
                <input id="key" type="text" />
            </p>
        </div>
        <div id="dlg-buttons"> 
                <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="onFilter()">确定</a>  
                <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#searchBox').dialog('close')">取消</a>  
        </div>
    </div>
    <script type="text/javascript">
        using("datagrid", function () {
            $("#tt").datagrid({
                url: "OptLogSrvController.aspx?method=GetPagedList",
                width: 780,
                height: 480,
                pagination: true,
                rownumbers: true,
                singleSelect: true,
                pageSize: 20,
                columns: [[
                     { field: "EvmCreateTime", title: "时间" ,width:80 }
                    , { field: "UserName", title: "操作员" ,width:120}
                    , { field: "Content", title: "操作内容" ,width:400 }
                    , { field: "IP", title: "IP",width:120 }
                ]],
                toolbar:[{
                    iconCls: 'icon-search',
                    text: '搜索',
                    handler: function () { onSearch(); }
                }]
            });
        });

        function onSearch() {
            $('#searchBox').css('display', 'block').dialog({
                title: '搜索',
                modal: true
            });
        }
        function onFilter() {
            $('#searchBox').dialog('close');
            $('#tt').datagrid('load', {userName:$('#userName').val(), key: $('#key').val() });
        }
    </script>
   </form>
</body>
</html>
