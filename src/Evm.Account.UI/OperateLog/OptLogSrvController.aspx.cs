﻿
//
//  OptLogSrv 控制器代码
//  由 Evm Aspx编译器生成于2011/9/3 11:36:43。
//  警告：请不要修改本文件，以防重新生成时被覆盖
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Evm.Rtl;
using Evm.Rtl.Web;
using System.Web.UI;
using Evm.AccountManagement.Model;
using Evm.AccountManagement.Service;

namespace Evm.AccountManagement.UI.OperateLog
{
    public partial class OptLogSrvController : Page
    {
        //Page_Load 方法，调度
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                switch (Request["method"])
                {
                    case "GetPagedList":
                        GetPagedList();
                        break;
                }
            }
            catch (Exception err)
            {
                JsonUtil.WriteJson(new InvokeUnknownException(err.Message));
            }
        }

        private void GetPagedList()
        {
            var pageIndex = string.IsNullOrEmpty(Request["page"]) ? 1 : Convert.ToInt32(Request["page"]);
            var pageSize = string.IsNullOrEmpty(Request["rows"]) ? 20 : Convert.ToInt32(Request["rows"]);
             var list = EvmAMApp.Instance.GetOptLogSrv().GetPagedList(Request["userName"], Request["key"],pageIndex,pageSize,"OperateLogID","desc");
             JsonUtil.WritePagedJson(list);
        }

    }
}





