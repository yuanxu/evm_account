﻿
<%@ Page Language="C#" AutoEventWireup="true"  ClientIDMode="Static"  EnableViewState="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    Evm.AccountManagement.WebAuth.Client.WebAuthHelper.CheckPermission("Organ", 1);
    var isAdd = string.IsNullOrEmpty(Request["OrganID"]);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Organ编辑器</title>
    <am:Style runat="server" />
    <script type="text/javascript">
        using("messager");
    </script>
</head>

<body>
    <form id="form1" method="post"  action='OrganSrvController.aspx?Method=<%=isAdd?"Add":"Save&OrganID="+Request["OrganID"]%>'>
    <div style='height:250px'>
        <p>
            <label for="OrganName">机构名称:</label>
            <input type="text" id="OrganName" name="OrganName"  class="easyui-validatebox"  required='true' vaildType='length[1,20]' />
        </p>
         <p>
            <label for="Memo">备注：</label>
            <input type='text' id='Memo' name='Memo'  class="easyui-validatebox" vaildType='length[1,50]' />
        </p>
        <p>
            <label for="ParentOrganID">上级机构:</label>
            <select id='ParentOrganID' name='ParentOrganID'></select>
        
        </p>
     </div>
     <div class='toolbar' style='text-align:right;'>
        <a href='#' class='easyui-linkbutton' iconCls="icon-ok" onclick='return onSubmit()'>确定</a>
        <a href='#' class='easyui-linkbutton' iconCls="icon-cancel" onclick='onCancel()'>取消</a>
    </div>
    </form>
    <script type="text/javascript">
        using("combotree", function () {
            $('#ParentOrganID').combotree({ url: '../Shared/Helper.aspx?method=GetOrganTree' 
            <%if(!isAdd){ %>
             ,onLoadSuccess:function(){ //组织机构加载后，加载需要编辑的数据
                using("form",function(){
                    $('#form1').form('load','OrganSrvController.aspx?method=GetByID&OrganID=<%=Request["OrganID"]+"&t="+DateTime.Now.Millisecond %>');
                    });
                }
        <%} %>
            });
        });
        
        function onSubmit() {
            $('#form1').submitForm();
        }
        function onCancel() {
            parent.closeEditor();
        }
    </script>
</body>
</html>





