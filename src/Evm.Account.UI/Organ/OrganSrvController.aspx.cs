﻿
//
//  OrganSrv 控制器代码
//  由 Evm Aspx编译器生成于2011/9/3 11:36:44。
//  警告：请不要修改本文件，以防重新生成时被覆盖
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Evm.Rtl;
using Evm.Rtl.Web;
using System.Web.UI;
using Evm.AccountManagement.Model;
using Evm.AccountManagement.Service;
using Evm.AccountManagement.WebAuth.Client;

namespace Evm.AccountManagement.UI.Organ
{
    [WebAuth(ResourceCode="Organ",Permission=1)]
    public partial class OrganSrvController : WebAuthProtectedPage
    {
        //Page_Load 方法，调度
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                switch (Request["method"])
                {

                    case "GetList":
                        GetChildren();
                        break;


                    case "GetByID":
                        GetByID();
                        break;


                    case "Add":
                        Add();
                        break;


                    case "Save":
                        Save();
                        break;


                    case "DeleteByID":
                        DeleteByID();
                        break;

                }
            }
            catch (EvmRtException err)
            {
                JsonUtil.WriteJson(err);
            }
            catch (Exception err)
            {
                JsonUtil.WriteJson(err);
            }
            finally
            {
                Response.End();
            }
        }


        private void GetChildren()
        {

            var organID = Convert.ToInt32(Request["OrganID"]);



            JsonUtil.WriteJson(EvmAMApp.Instance.GetOrganSrv().GetChildren(organID));
        }



        private void GetByID()
        {

            var id = Convert.ToInt32(Request["OrganID"]);



            JsonUtil.WriteJson(EvmAMApp.Instance.GetOrganSrv().GetByID(id));
        }



        private void Add()
        {

            var userName = Request["userName"];


            var organ = new Model.Organ();


            GetOrgan(organ);

            EvmAMApp.Instance.GetOrganSrv().Add(userName, organ);
            JsonUtil.WriteSuccessReponse();
        }

        private void GetOrgan(Model.Organ obj)
        {
            obj = obj == null ? new Model.Organ() : obj;

            if (!string.IsNullOrEmpty(Request["OrganName"]))
                obj.OrganName = Request["OrganName"];


            if (!string.IsNullOrEmpty(Request["Memo"]))
                obj.Memo = Request["Memo"];


            if (!string.IsNullOrEmpty(Request["ParentOrganID"]))
                obj.ParentOrganID = Convert.ToInt32(Request["ParentOrganID"]);


        }



        private void Save()
        {

            var userName = Request["userName"];

            var organ = EvmAMApp.Instance.GetOrganSrv().GetByID(int.Parse(Request["OrganID"]));
GetOrgan(organ);

            EvmAMApp.Instance.GetOrganSrv().Save(userName, organ);
            JsonUtil.WriteSuccessReponse();
        }




        private void DeleteByID()
        {

            var userName = Request["userName"];


            var organID = Convert.ToInt32(Request["organID"]);


            var delChildren = Convert.ToBoolean(Request["delChildren"]);

            EvmAMApp.Instance.GetOrganSrv().DeleteByID(userName, organID, delChildren);
            JsonUtil.WriteSuccessReponse();
        }


    }
}


