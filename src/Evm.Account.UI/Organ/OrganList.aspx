﻿<%@ Page Language="C#" AutoEventWireup="true"  ClientIDMode="Static"  EnableViewState="false" %>
<%Evm.AccountManagement.WebAuth.Client.WebAuthHelper.CheckPermission("Organ",1); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <am:Style runat="server" />
    <script type="text/javascript">
    using(["messager","dialog"])
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <div id="tt"></div>
    <div id='editor' style='width:450px;height:350px;display:none;overflow:visible' url="OrganEditor.aspx?pOrgan=<%=Request["OrganID"] %>" title='机构信息' ></div>
    <script type="text/javascript">
        using("datagrid", function () {
            $("#tt").datagrid({
                url: 'OrganSrvController.aspx?method=GetList&OrganID=<%=Request["OrganID"] %>',
                width: 780,
                height: 480,
                pagination: true,
                rownumbers: true,
                singleSelect: true,
                pageSize: 20,
                columns: [[
                     { field: "OrganName", title: "机构名称",width:120 }
                    , { field: "Memo", title: "备注" ,width:300}
                ]]
                 , toolbar: [{
                    iconCls: 'icon-add',
                    text: '新建',
                    handler: function () { onAdd(); }
                }, {
                    iconCls: 'icon-edit',
                    text: '修改',
                    handler: function () { onEdit(); }
                }, {
                    iconCls: 'icon-cut',
                    text: '删除',
                    handler: function () { onDelete(); }
                }]
            });
        });
        function onAdd()
        {
         $('#editor').editor('add');
        }
        function onEdit() {
            $('#editor').editor('edit', {
                gridId: 'tt'
                 , idField: 'OrganID'
            });

        }
        function onDelete() {
            $('#editor').editor('delete', {
                url: 'OrganSrvController.aspx?method=DeleteByID'
                , gridId: 'tt'
                , idField: 'OrganID'
                , nameField: 'OrganName'
            });
        }
        function closeEditor(reload) {
            $('#editor').dialog('close');
            if (reload) {
                $("#tt").datagrid('reload');
            }
        }
    </script>
   </form>
</body>
</html>
