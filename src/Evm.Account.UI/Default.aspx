﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Evm.AccountManagement.UI.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <am:Style ID="Style1" runat="server" />
</head>
<body class="easyui-layout" style="text-align:left">
   <div style="width:1000px;height:700px;margin:0 auto">
    <div region="north" border="false" style="background:#666;text-align:center">        <p><br />
        Evm Account Management System v1.0
        <br />
        </p>
	</div>
    <div region="west" border="false" split="true" style="width:200px;padding:5px;float:left;background:#fafafa;height:100%">        <div class="easyui-accordion" id='mm'>            <div title='日常工作'>                <ul class='mm'>                    <li>                        <span>                            <a href='#' onclick='openUrl("用户管理","Account/AccountList.aspx")'>用户管理</a>                        </span>                    </li>                    <li>                        <a href="#" onclick='openUrl("角色","Role/RoleList.aspx")'>角色</a>                    </li>                                       <li>                        <a href="#" onclick='openUrl("操作日志","OperateLog/OperateLogList.aspx")'>操作日志</a>                    </li>                </ul>            </div>            <div title='资源/应用'>                <ul class='mm'>                    <li><a href='#' onclick='openUrl("应用管理","Was/WasList.aspx")' >应用管理</a> </li>                     <li>                        <a href='#' onclick='openUrl("资源","Resource/ResourceList.aspx")'>资源</a>                    </li>                </ul>            </div>        </div>                    </div>
    <div region="center" border="false" style="float:left;width:790px;height:100%">
			<div id="tt" class="easyui-tabs" fit="true" border="false" plain="true" >
				<div title="Welcome" href="welcome.aspx" closable='true'></div>
			</div>
		</div>
</div>   
 <script type="text/javascript">
     using('tabs', function () {

         $('#tt').tabs();
     });
     function openUrl(title, url, reload) {
         if ($('#tt').tabs('exists', title)) {
             var tab = $('#tt').tabs('select', title);
             if (reload) {
                 tab.find("iframe").attr('src', url);

             }
         } else {
             $('#tt').tabs('add', {
                 title: title,

                 closable: true,
                 cache: true,
                 content:
                    "<iframe FRAMEBORDER='no' SCROLLING ='no' height='100%' width='100%' src='" + url + "'></iframe>"

             });
         }
     }
     function onSelectOrgan(node) {

         openUrl('组织机构', 'Organ/OrganList.aspx?method=GetList&OrganID=' + node.id, true);
     }
     using('accordion', function () {
         $('#mm').accordion();
     });
        using("tree", function () { 
           $('#organTree').tree({
                url:'Shared/Helper.aspx?method=GetOrganTree'
                , onClick: onSelectOrgan
            });
        });
    </script>
</body>
</html>
