﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Evm.AccountManagement.Service;
using Evm.Rtl.Web;
using Evm.Rtl;
using Evm.AccountManagement.WebAuth.Client;

namespace Evm.AccountManagement.UI.Was
{
    [WebAuth(ResourceCode="App",Permission=1)]
    public partial class WasController :WebAuthProtectedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                switch (Request["method"])
                {
                    case "GetList":
                        GetList();
                        break;
                    case "Add":
                        Add();
                        break;
                    case "Save":
                        Save();
                        break;
                    case "GetByID":
                        GetByID();
                        break;
                    case "DeleteByID":
                        DeleteByID();
                        break;
                }
            }
            catch (EvmRtException err)
            {
                JsonUtil.WriteJson(err);
            }
            catch (Exception err)
            {
                JsonUtil.WriteJson(err);
            }
            finally
            {
                Response.End();
            }
        }

        private void Add()
        {
            var was = new Model.Was();
            GetObject(was);
            EvmAMApp.Instance.GetWasSrv().Add(was);
            JsonUtil.WriteSuccessReponse();
        }

        private void DeleteByID()
        {
            EvmAMApp.Instance.GetWasSrv().DeleteByID(int.Parse(Request["WasID"]));
            JsonUtil.WriteSuccessReponse();
        }

        private void Save()
        {
            var was = EvmAMApp.Instance.GetWasSrv().GetByID(int.Parse(Request["WasID"]));
            GetObject(was);
            EvmAMApp.Instance.GetWasSrv().Save(was);
            JsonUtil.WriteSuccessReponse();
        }

        private void GetByID()
        {
            var was = EvmAMApp.Instance.GetWasSrv().GetByID(int.Parse(Request["WasID"]));
            JsonUtil.WriteJson(was);
        }

        private void GetList()
        {
            JsonUtil.WriteJson(EvmAMApp.Instance.GetWasSrv().GetList());
        }
        private Model.Was GetObject(Model.Was was)
        {
            was.WasCode = Request["WasCode"];
            was.FriendlyName = Request["FriendlyName"];
            was.Secret = Request["Secret"];
            was.Memo = Request["Memo"];
            return was;
        }
    }
}