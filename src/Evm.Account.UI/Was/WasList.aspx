﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WasList.aspx.cs" Inherits="Evm.AccountManagement.UI.Was.WasList" %>
<%Evm.AccountManagement.WebAuth.Client.WebAuthHelper.CheckPermission("App",1); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <am:Style runat="server" />
    <script type="text/javascript">
        using(["messager", 'dialog']);
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id='tt' class='easyui-datagrid' ></div>

    <div id='editor' style='width:460px;height:400px;display:none;overflow:visible' url='WasEditor.aspx' title='应用程序信息' >
        
    </div>
    <script type="text/javascript">
        using("datagrid", function () {
            $("#tt").datagrid({
                url: "WasSrvController.aspx?method=GetList",
                width: 780,
                height: 480,
                pagination: true,
                rownumbers: true,
                singleSelect: true,
                pageSize: 20,
                columns: [[
                      { field: "WasCode", title: "WAS", width: 120 }
                    , { field: "FriendlyName", title: "友好名称", width: 120 }
                    , { field: "Secret", title: "私钥", width: 100 }
                    , { field: "Memo", title: "备注", width: 300 }
                ]]
                , toolbar: [{
                    iconCls: 'icon-add',
                    text: '新建',
                    handler: function () { onAdd(); }
                }, {
                    iconCls: 'icon-edit',
                    text: '修改',
                    handler: function () { onEdit(); }
                }, {
                    iconCls: 'icon-cut',
                    text: '删除',
                    handler: function () { onDelete(); }
                }]
            });
        });
        function onAdd() {
            $('#editor').editor('add');
        }
        function onEdit() {
            $('#editor').editor('edit', { idField: 'WasID', gridId: 'tt' });
        }
        function onDelete() {
            $('#editor').editor('delete', {
                url: 'WasSrvController.aspx?method=DeleteByID'
                , idField: 'WasID'
                , gridId: 'tt'
                , nameField: 'FriendlyName'
            });
        }
        function closeEditor(reload) {
            $('#editor').dialog('close');
            if (reload) {
                $('#tt').datagrid('reload');
            }
        }
    </script>
    </form>
</body>
</html>
