﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WasEditor.aspx.cs" Inherits="Evm.AccountManagement.UI.Was.WasEditor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    Evm.AccountManagement.WebAuth.Client.WebAuthHelper.CheckPermission("App", 1);
    var isAdd = string.IsNullOrEmpty(Request["WasID"]);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <am:Style runat="server" />
</head>
<body>
    <form id="form1" method="post" action='WasSrvController.aspx?method=<%= isAdd ? "Add" : "Save&WasID="+Request["WasID"]%>'>
    <div>
    <p>
        <label>代码：</label>
        <input id="WasCode" name="WasCode" type="text"  class="easyui-validatebox" required='true' vaildType='length[1,20]'   />
    </p>

    <p>
        <label>名称：</label>
        <input id="FriendlyName" name="FriendlyName" type="text"  class="easyui-validatebox" required='true' vaildType='length[1,100]'  />
    </p>

    <p>
        <label>密码：</label>
        <input id="Secret" name="Secret" type="text" class="easyui-validatebox" required='true' vaildType='length[16]' />
        <input id='autoGen' type="button" value='自动生成' onclick='doAutoGen()' />
        <br />
        <label></label>
        <span class="gray"> 16个字符，可以是字母、数字、特殊字符等.</span>
    </p>
    <p>
        <label>状态：</label>
        <select name="Status" id="Status">
            <option value="0">正常</option>
            <option value="1">已禁止</option>
            <option value="2">已过期</option>
        </select>
    </p>
    <p>
        <label>备注：</label>
        <textarea id="Memo" name="Memo" rows="5" cols="20"  class="easyui-validatebox" vaildType='length[1,300]' ></textarea>
    </p>
    </div>

    <p class="toolbar">
         <a href='#' class='easyui-linkbutton' iconCls="icon-ok" onclick='return onSubmit()'>确定</a>
            <a href='#' class='easyui-linkbutton' iconCls="icon-cancel" onclick='onCancel()'>取消</a>
    </p>
    </form>
    <script type="text/javascript">
        function onSubmit() {
            $('#form1').submitForm();
        }
        function onCancel() {
            parent.closeEditor();
        }
        function doAutoGen() {
            var result="";
            while(result.length <16)
            {
                var v = randRange(49,122);
                if(v>=58 && v <=64  //: ; < = > ? @
                || v>=91 && v <=96 //[ \ ] ^ _ `
                )
                continue;
                result += String.fromCharCode(v);
            }
         $('#Secret').val(result);   
        }
        function randRange(min, max) {
            var randomNum = Math.floor(Math.random() * (max - min + 1)) + min;
            return randomNum;
        }
         <%
        if(!isAdd)
        {
         %>
         using("form",function(){
            $("#form1").form('load','WasSrvController.aspx?method=GetByID&WasID=<%=Request["WasID"]+"&t="+DateTime.Now.Millisecond %>');
         });
         <%} %>
    </script>
</body>
</html>
