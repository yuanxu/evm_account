﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Style.ascx.cs" Inherits="Evm.AccountManagement.UI.Style.MainStyle" %>
<%var appPath = Request.ApplicationPath; %>
<link href="<%=appPath %>Style/main.css" rel="stylesheet" type="text/css" />
<link href="<%=appPath %>Scripts/jquery-easyui-1.2.4/themes/icon.css" rel="stylesheet" type="text/css" />
<script src="<%=appPath %>Scripts/jquery-easyui-1.2.4/jquery-1.6.min.js" type="text/javascript"></script>
<script src="<%=appPath %>Scripts/jquery-easyui-1.2.4/easyloader.js" type="text/javascript"></script>
<script src="<%=appPath %>Scripts/EvmUtils.js" type="text/javascript"></script>
<script type="text/javascript">
    easyloader.base = "<%=appPath %>Scripts/jquery-easyui-1.2.4/";
    easyloader.theme = "gray";
    easyloader.locale = 'zh_CN';
</script>