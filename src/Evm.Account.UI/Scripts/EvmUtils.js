﻿/*
    Evm 客户端工具类
*/
//编辑器相关
using(["form", "messager", "validate"]);
(function ($) {

    var methods = {
        init: function () {
        },
        add: function (url) {
            doInit(this, { url: url });
            this.css('display', 'block').dialog();
        },
        edit: function (options) {
            var settings = doInit(this, options);
            if (settings.gridId == undefined) {
                alert('请提供gridId属性！');
            }
            var row = $("#" + settings.gridId).datagrid('getSelected');
            if (row == null) {
                $.messager.alert('提示', '请选择需要修改的行！', 'warning');
                return false;
            }
            var idValue = eval("(row." + settings.idField + ")");
            var tUrl = settings.url;
            if (tUrl.indexOf("?") >= 0)
                tUrl += "&";
            else
                tUrl += "?";
            tUrl += settings.idField + "=" + idValue

            this.find("iframe").attr("src", tUrl);
            this.css('display', 'block').dialog();
        },
        delete: function (options) {
            var settings = {
                url: undefined
            , idField: 'id'
            , nameField: undefined
            , gridId: undefined
            };

            if (options) {
                $.extend(settings, options);
            }
            if (settings.gridId == undefined) {
                alert('请提供表格控件的ID属性(gridId)!');
            }
            var row = $("#" + settings.gridId).datagrid('getSelected');
            if (row == null) {
                $.messager.alert('提示', '请选择要删除的行！', 'warning');
                return false;
            }
            var idValue = eval("(row." + settings.idField + ")");
            var tUrl = settings.url;
            if (tUrl.indexOf("?") >= 0)
                tUrl += "&";
            else
                tUrl += "?";
            tUrl += settings.idField + "=" + idValue
            var name = "";
            if (settings.nameField != undefined)
                name = eval("(row." + settings.nameField + ")");
            $.messager.confirm("确认", "您确定要删除'" + name + "'吗？", function (b) {
                if (!b)
                    return;
                $.post(tUrl
                    , function (data, textStatus) {
                        var err = $.parseJSON(data);
                        if (err.IsSucceed) {
                            //从数据表中删除 
                            var index = $('#'+settings.gridId).datagrid('getRowIndex', row);
                            $('#'+settings.gridId).datagrid('deleteRow', index);
                        }
                        else
                            $.messager.alert("失败", "删除数据失败！" + err.ErrorMessage, "error");
                    });
            });
        },
        close:function(){
            this.dialog('close');
        }
    }
    function doInit(obj, options) {
        var settings = {
            url: obj.attr('url')
            , idField: 'id'
            , gridId: undefined
        };

        if (options) {
            $.extend(settings, options);
        }
        var url = settings.url;

        if (url == undefined)
            url = obj.attr("url");
        else
            obj.attr("url", url); //保存url
        var ifrm = obj.find("iframe");
        if (ifrm.length == 0) {
            obj.html('<iframe  src="" frameborder="0" width="100%" height="98%" scrolling="no" ></iframe>');
            ifrm = obj.find("iframe");
        }
        ifrm.attr("src", url);
        return settings;
    }
    //
    $.fn.editor = function (method) {
        // Method calling logic
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.editor');
        }

    }
})(jQuery);

//form
(function ($) {
    $.fn.submitForm = function(){
        var obj = this;
         this.form('submit', {
                onSubmit: function () {
                    return obj.form('validate');
                }
                , success: function (data) {
                    var jo = eval("(" + data + ")");
                    if (jo.IsSucceed)
                        parent.closeEditor(true);
                    else
                        alert(jo.ErrorMessage);

                }
            });
    }
})(jQuery);