﻿
//
//  ResourceSrv 控制器代码
//  由 Evm Aspx编译器生成于2011/9/3 11:36:44。
//  警告：请不要修改本文件，以防重新生成时被覆盖
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Evm.Rtl;
using Evm.Rtl.Web;
using System.Web.UI;
using Evm.AccountManagement.Model;
using Evm.AccountManagement.Service;
using Evm.AccountManagement.WebAuth.Client;

namespace Evm.AccountManagement.UI.Resource
{
    [WebAuth(ResourceCode="Resource", Permission=1)]
    public partial class ResourceSrvController : WebAuthProtectedPage
    {
        //Page_Load 方法，调度
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                switch (Request["method"])
                {
                    case "GetPagedList":
                        GetPagedList();
                        break;
                    case "Add":
                        Add();
                        break;
                    case "GetByID":
                        GetByID();
                        break;
                    case "Save":
                        Save();
                        break;
                    case "DeleteByID":
                        DeleteByID();
                        break;

                }
            }
            catch (EvmRtException err)
            {
                JsonUtil.WriteJson(err);
            }
            catch (Exception err)
            {
                JsonUtil.WriteJson(err);
            }
            finally
            {
                Response.End();
            }
        }

       


        private void GetPagedList()
        {

            var pageIndex = string.IsNullOrEmpty(Request["page"]) ? 1 : Convert.ToInt32(Request["page"]);


            var pageSize = string.IsNullOrEmpty(Request["rows"]) ? 20 : Convert.ToInt32(Request["rows"]);


            var sortColumn = Request["sortColumn"];


            var sortDirection = Request["sortDirection"];



            JsonUtil.WritePagedJson(EvmAMApp.Instance.GetResourceSrv().GetPagedList(pageIndex, pageSize, sortColumn, sortDirection));
        }



        private void Add()
        {
            var userName = Request["userName"];
            var res = new Model.Resource();
            GetResource(res);
            IList<KeyValuePair<int, int>> rolePermission = null;
            EvmAMApp.Instance.GetResourceSrv().Add(userName, res, rolePermission);
            JsonUtil.WriteSuccessReponse();
        }

        private void GetResource(Model.Resource obj)
        {
            obj = obj == null ? new Model.Resource() : obj;
            if (!string.IsNullOrEmpty(Request["ResourceCode"]))
                obj.ResourceCode = Request["ResourceCode"];
            if (!string.IsNullOrEmpty(Request["ResourceName"]))
                obj.ResourceName = Request["ResourceName"];
            if (!string.IsNullOrEmpty(Request["Permissions"]))
                obj.Permissions = Request["Permissions"];
            if (!string.IsNullOrEmpty(Request["Memo"]))
                obj.Memo = Request["Memo"];
            if (!string.IsNullOrEmpty(Request["WasCode"]))
                obj.WasCode = Request["WasCode"];
        }

        private void GetByID()
        {

            var id = Convert.ToInt32(Request["ResourceID"]);

            JsonUtil.WriteJson(EvmAMApp.Instance.GetResourceSrv().GetByID(id));
        }

        private void Save()
        {
            var userName = Request["userName"];

            var res = EvmAMApp.Instance.GetResourceSrv().GetByID(int.Parse(Request["ResourceID"]));

            GetResource(res);

            IList<KeyValuePair<int, int>> rolePermission = null;

            EvmAMApp.Instance.GetResourceSrv().Save(userName, res, rolePermission);
            JsonUtil.WriteSuccessReponse();
        }

        private void DeleteByID()
        {
            var userName = Request["userName"];

            var resID = Convert.ToInt32(Request["ResourceID"]);

            EvmAMApp.Instance.GetResourceSrv().DeleteByID(userName, resID);
            JsonUtil.WriteSuccessReponse();
        }

    }
}




