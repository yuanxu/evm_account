﻿<%@ Page Language="C#" AutoEventWireup="true"  ClientIDMode="Static"  EnableViewState="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%Evm.AccountManagement.WebAuth.Client.WebAuthHelper.CheckPermission("Resource",1); %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <am:Style runat="server" />
    <script type="text/javascript">
        using(["messager",'dialog']);
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <div id="tt"></div>
    <div id='editor' style='width:460px;height:500px;display:none;overflow:visible' url='ResourceEditor.aspx' title='资源信息' >
        
    </div>
    <script type="text/javascript">
        using("datagrid", function () {
            $("#tt").datagrid({
                url: "ResourceSrvController.aspx?method=GetPagedList",
                width: 780,
                height: 480,
                pagination: true,
                rownumbers: true,
                singleSelect: true,
                pageSize: 20,
                columns: [[
                      { field: "WasCode", title: "WAS", width: 60 }
                    , {field:"ResourceCode",title:'资源编码',width:60}
                    , { field: "ResourceName", title: "资源名", width: 120 }
                    , { field: "Permissions", title: "权限表", width: 400 }
                    , { field: "Memo", title: "备注", width: 200 }
                ]]
                , toolbar: [{
                    iconCls: 'icon-add',
                    text: '新建',
                    handler: function () { onAdd(); }
                }, {
                    iconCls: 'icon-edit',
                    text: '修改',
                    handler: function () { onEdit(); }
                }, {
                    iconCls: 'icon-cut',
                    text: '删除',
                    handler: function () { onDelete(); }
                }]
            });
        });
        function onAdd()
        {
            $('#editor').editor('add');
        }
         function onEdit() {
             $('#editor').editor('edit', {idField:'ResourceID',gridId:'tt' });
        }
        function onDelete()
        {
            $('#editor').editor('delete', {
                url: 'ResourceSrvController.aspx?method=DeleteByID'
                , idField: 'ResourceID'
                ,gridId:'tt'
                ,nameField:'ResourceName'
            });
        }
            function closeEditor(reload) {
                $('#editor').dialog('close');
                if (reload) {
                    $('#tt').datagrid('reload');
                }
            }
    </script>
   </form>
</body>
</html>
