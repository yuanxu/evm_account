﻿
<%@ Page Language="C#" AutoEventWireup="true"  ClientIDMode="Static"  EnableViewState="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    Evm.AccountManagement.WebAuth.Client.WebAuthHelper.CheckPermission("Resource", 1);
    var isAdd = string.IsNullOrEmpty(Request["ResourceID"]);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Resource编辑器</title>
    <am:Style runat="server" />
    <script type="text/javascript">
        using(["messager","form","tabs"]);
    </script>
</head>

<body>
    <form id="form1" method="post"  action='ResourceSrvController.aspx?Method=<%=isAdd?"Add":"Save&ResourceID="+Request["ResourceID"]%>'>
    <div id="tt" class="easyui-tabs" fit="true" border="false" plain="true" style="height:360px"  >
        <div title='基本信息'>
            <p>
                <label for="WasCode">WasCode:</label>
                <select id="WasCode" name="WasCode"  class="easyui-validatebox" required='true'></select>
            </p>
            <p>
                <label for="ResourceCode">资源编码：</label>
                <input type="text" name="ResourceCode" id="ResourceCode" class="easyui-validatebox" required='true' validType="length[1,50]" />
            </p>
            <p>
                <label for="ResourceName">资源名:</label>
                <input type="text" id="ResourceName" name="ResourceName"  class="easyui-validatebox" required='true' validType="length[1,50]" />
            </p>
            <p>
                <label for="Permissions">权限表:</label>
                <textarea id="Permissions" name="Permissions"  rows="5" cols="50" class="easyui-validatebox" validType="length[1,500]"></textarea>
                <span class='gray'><br />
                    以';'分隔，最多可以声明32个权限。<br />
                    设定完成后调整权限定义或顺序有可能引起系统权限错乱，请慎重调整！
                </span>
            </p>
            <p>
                <label for="Memo">备注:</label>
                <textarea id="Memo" name="Memo"  rows="3" cols="50" class="easyui-validatebox" validType="length[1,300]"></textarea>
            </p>
        </div>
        <div title='授权角色'>
            <br />
                <div id='dt'></div>
            </div>
        
        </div>
        <p class="toolbar">
           <a href='#' class='easyui-linkbutton' iconCls="icon-ok" onclick='return onSubmit()'>确定</a>
            <a href='#' class='easyui-linkbutton' iconCls="icon-cancel" onclick='onCancel()'>取消</a>
        </p>
    
    </form>
    <script type="text/javascript">
        function onSubmit() {
            $('#form1').submitForm();
        }
        function onCancel() {
            parent.closeEditor();
        }
        using("tabs",function(){$("#tt").tabs("");s });
        using("combobox",function(){
            $("#WasCode").combobox({
             url:'../Shared/Helper.aspx?method=GetWasList',
                valueField:'id',  
                textField:'text' 
       ,onLoadSuccess:function(){ //WasList加载后，加载需要编辑的数据
        <%
        if(!isAdd)
        {
         %>
         using("form",function(){
            $("#form1").form(
                { onLoadSuccess:function(data){//数据加载完成后，初始化相应的几个combobx字段
                    using("combotree",function(){$('#WasCode').combotree('setValue',data.WasCode);});                  }                }
            ).form('load','ResourceSrvController.aspx?method=GetByID&ResourceID=<%=Request["ResourceID"]+"&t="+DateTime.Now.Millisecond %>');
         });
         using("datagrid",function(){
            $('#dt').datagrid({
             url:'../Shared/Helper.aspx?method=GetAssignedRoles&ResourceID=<%=Request["ResourceID"] %>',
             height:230,

                rownumbers: true,
                singleSelect: true,
             columns:[[
                {field:"RoleName",title:"角色",width:100},
                {field:"Permissions",title:"权限",width:280}
             ]]
            });
         });
         <%} %>

         }
             });
        });
         
    </script>
</body>
</html>





