﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Evm.AccountManagement.Service;
using Evm.Rtl;
using Evm.Rtl.Web;
using System.Text;

namespace Evm.AccountManagement.UI.Shared
{
    public partial class Helper : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            switch (Request["method"])
            {
                case "GetOrganUserList":
                    var accID = Convert.ToInt32(Request["AccountID"]);
                    JsonUtil.WriteJson(ToJson(
                            EvmAMApp.Instance.GetOrganSrv().GetAccounts(Convert.ToInt32(Request["organID"]))
                            , acc => acc.AccountID == accID ? "" : (string.Format("{{\"id\":\"{0}\",\"text\":\"{1}\" }}", acc.AccountID, acc.Name))
                                ));
                    break;
                case "GetOrganTree":
                    JsonUtil.WriteJson(GetOrganTree(EvmAMApp.Instance.GetOrganSrv().GetChildren(0)));
                    break;
                case "GetOrganChildren":
                    GetOrganChildren();
                    break;
                case "GetOrganRoleList":
                    JsonUtil.WriteJson(ToJson(
                        EvmAMApp.Instance.GetOrganSrv().GetPosts(Convert.ToInt32(Request["organID"]))
                         , r => string.Format("{{\"id\":\"{0}\",\"text\":\"{1}\" }}", r.RoleID, r.Role.RoleName))
                        );
                    break;
                case "GetOrganRoleData":
                    JsonUtil.WriteJson(GetOrganPostData(Convert.ToInt32(Request["organID"]), Convert.ToInt32(Request["AccountID"])));
                    break;
                case "GetOtherOrganRoleData":
                    JsonUtil.WriteJson(GetOtherOrganRoleData(Convert.ToInt32(Request["organID"]), Convert.ToInt32(Request["AccountID"])));
                    break;
                case "GetAssignedRoles":
                    GetAssignedRoles();
                    break;
                case "GetAccountsByRoleID":
                    GetAccountsByRoleID();
                    break;
                case "GetAclsByRoleID":
                    GetAclsByRoleID();
                        break;
                case "GetOrgansByRoleID":
                        GetOrgansByRoleID();
                        break;
                case "GetWasList":
                        JsonUtil.WriteJson(EvmAMApp.Instance.GetWasSrv().GetList());
                        JsonUtil.WriteJson(ToJson(
                          EvmAMApp.Instance.GetWasSrv().GetList()
                           , w => string.Format("{{\"id\":\"{0}\",\"text\":\"{1}\" }}",w.WasCode,w.WasCode))
                          );
                        break;
                default:
                    break;
            }
        }

        private void GetOrganChildren()
        {
            var id =string.IsNullOrEmpty(Request["id"]) ? 0: int.Parse(Request["id"]);
            var childern = EvmAMApp.Instance.GetOrganSrv().GetChildren(id);
            var builder = new StringBuilder();
            builder.Append("[");
            var isFirst = true;
            foreach (var item in childern)
            {
                if (!isFirst)
                {
                    builder.Append(",");
                }
                else
                {
                    isFirst = false;
                }
                
                builder.AppendFormat("{{\"id\":{0},\"text\":\"{1}\"{2} }}",
                    item.OrganID, item.OrganName, ",\"status\":\"closed\"" );
            }
            builder.AppendLine("]");
            JsonUtil.WriteJson(builder.ToString());
        }

        private void GetOrgansByRoleID()
        {
            if (string.IsNullOrEmpty(Request["RoleID"]))
                return;
            var list = EvmAMApp.Instance.GetPostSrv().GetListByRoleID(int.Parse(Request["RoleID"]));
            var result = new StringBuilder();
            foreach (var item in list)
            {
                if (result.Length != 0)
                    result.Append(";");
                result.Append(item.OrganID);
            }
            Response.Write(result);
        }

        private void GetAclsByRoleID()
        {
           var list = EvmAMApp.Instance.GetAclSrv().GetResources(int.Parse(Request["RoleID"]));
           JsonUtil.WriteJson(list);
        }

        private void GetAccountsByRoleID()
        {
            var list = EvmAMApp.Instance.GetAccountSrv().GetListByRoleID(int.Parse(Request["RoleID"]));
            JsonUtil.WriteJson(list);
        }

        /// <summary>
        /// 获取部门的岗位.PagedJson
        /// </summary>
        /// <param name="organID"></param>
        /// <param name="AccountID"></param>
        /// <returns></returns>
        private string GetOrganPostData(int organID, int accID)
        {
            return FormatHoldRole(EvmAMApp.Instance.GetOrganSrv().GetPosts(organID), accID);
        }

        
        /// <summary>
        /// 获取其他部门的岗位.PagedJson
        /// </summary>
        /// <param name="organID"></param>
        /// <param name="AccountID"></param>
        /// <returns></returns>
        private string GetOtherOrganRoleData(int organID,int accID)
        {
            var list = EvmAMApp.Instance.GetPostSrv().GetOtherPosts(organID);
            return FormatHoldRole(list, accID);
        }
        private string FormatHoldRole(IList<Model.Post> posts,int accID)
        {
            IList<Model.HoldPost> holdPosts = EvmAMApp.Instance.GetHoldPostSrv().GetListByAccountID(accID);
            var result = new List<Object>();
            var cache = Cache.Get("cachedOrganName");
            if (cache != null)
            {
                cachedOrganName = (IDictionary<int, string>)cache;
            }
            foreach (var post in posts)
            {
                var holdExp = from hp in holdPosts
                              where hp.PostID == post.PostID
                              select hp;
                result.Add(new
                {
                    PostID = post.PostID,
                    RoleName = post.Role.RoleName,
                    Memo = post.Role.Memo,
                    OrganName = GetOrganName(post.OrganID)
                    ,
                    IsHold = holdExp.Count() > 0,
                    IsConcurrentPost = (holdExp.Count() == 1 && holdExp.First().IsConcurrentPost)
                });
            }
            if (cache == null)
            {
                Cache.Insert("cachedOrganName", cachedOrganName);
            }
            return result.ToPagedJson();
        }
        private static IDictionary<int, string> cachedOrganName = new Dictionary<int, string>();
        private string GetOrganName(int organID)
        {
            
            if (cachedOrganName.ContainsKey(organID))
                return cachedOrganName[organID];
            var organ = EvmAMApp.Instance.GetOrganSrv().GetByID(organID);
            if (organ.ParentOrganID == 0)
            {
                return organ.OrganName;
            }
            else
            {
                var organName =  GetOrganName(organ.ParentOrganID) + " - "+organ.OrganName;
                cachedOrganName.Add(organID, organName);
                return organName;
            }
        }

        private string ToJson<T>(IList<T> list, Func<T, string> func)
        {
            var builder = new StringBuilder();
            foreach (var item in list)
            {
                var result = func(item);
                if (!string.IsNullOrEmpty(result))
                {
                    if (builder.Length > 0)
                        builder.Append(",");
                    builder.AppendLine(result);
                }
            }
            return "[" + builder.ToString() + "]";
        }
        private void GetAssignedRoles()
        {
            var resID = int.Parse(Request["ResourceID"]);
            var list = EvmAMApp.Instance.GetAclSrv().GetListByResourceID(resID);
            var result = new System.Collections.ArrayList();
            
            foreach (var acl in list)
            {
                var permissions = new StringBuilder();
                var res = EvmAMApp.Instance.GetResourceSrv().GetByID(acl.ResourceID);
                if (string.IsNullOrEmpty(res.Permissions))
                    continue;
                var parts = res.Permissions.Split(new char[] { ';' });
                for (var i = 0; i < parts.Length; i++)
                {
                    if (((int)(Math.Pow(2, i)) & acl.Permission) > 0)
                    {
                        if (permissions.Length > 0)
                            permissions.Append(";");
                        permissions.Append(parts[i]);
                    }
                }
                
                result.Add(new {RoleName=EvmAMApp.Instance.GetRoleSrv().GetByID(acl.RoleID).RoleName
                    ,Permissions= permissions.ToString()    }
                    );
                
            }
            JsonUtil.WriteJson(result);
            //JsonUtil.WriteJson(string.Format("[{{\"total\":{0},\"rows\":{1} }}]",count, result));
        }
        private string GetOrganTree(IList<Model.Organ> list)
        {
            var builder = new StringBuilder();
            builder.Append("[");
            var isFirst = true;
            foreach (var item in list)
            {
                if (!isFirst)
                {
                    builder.Append(",");
                }
                else
                {
                    isFirst = false;
                }
                var childern = EvmAMApp.Instance.GetOrganSrv().GetChildren(item.OrganID);
                builder.AppendFormat("{{\"id\":{0},\"text\":\"{1}\"{2}, \"status\":\"closed\" }}",
                    item.OrganID, item.OrganName, childern == null || childern.Count == 0 ? "" : ",\"children\":" + GetOrganTree(childern));
            }
            builder.AppendLine("]");
            return builder.ToString();
        }
    }
}