﻿
//
//  RoleSrv 控制器代码
//  由 Evm Aspx编译器生成于2011/9/3 11:36:44。
//  警告：请不要修改本文件，以防重新生成时被覆盖
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Evm.Rtl;
using Evm.Rtl.Web;
using System.Web.UI;
using Evm.AccountManagement.Model;
using Evm.AccountManagement.Service;
using System.Text;
using Evm.AccountManagement.WebAuth.Client;

namespace Evm.AccountManagement.UI.Role
{
    [WebAuth(ResourceCode="Role",Permission=1)]
    public partial class RoleSrvController : WebAuthProtectedPage
    {
        //Page_Load 方法，调度
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                switch (Request["method"])
                {

                    case "GetPagedList":
                        GetPagedList();
                        break;


                    case "GetByID":
                        GetByID();
                        break;


                    case "Add":
                        Add();
                        break;


                    case "Save":
                        Save();
                        break;


                    case "DeleteByID":
                        DeleteByID();
                        break;
                    case "GetAclsByRoleID":
                        GetAclsByRoleID();
                        break;
                    case "GetResources":
                        GetResources();
                        break;
                }
            }
            catch (EvmRtException err)
            {
                JsonUtil.WriteJson(err);
            }
            catch (Exception err)
            {
                JsonUtil.WriteJson(err);
            }
            finally
            {
                Response.End();
            }
        }

        private void GetResources()
        {
            var reslist = EvmAMApp.Instance.GetResourceSrv().GetList();
            JsonUtil.WriteJson(reslist);
        }

        /// <summary>
        /// 获取所有权限
        /// </summary>
        private void GetAclsByRoleID()
        {
            var roleID = string.IsNullOrEmpty(Request["RoleID"]) ? 0 : Convert.ToInt32(Request["RoleID"]);
            var acls = EvmAMApp.Instance.GetAclSrv().GetListByRoleID(roleID);
            var resources = EvmAMApp.Instance.GetResourceSrv().GetList().OrderBy(res=>res.WasCode);
            var result = new List<object>();
            foreach (var res in resources)
            {
                if (string.IsNullOrEmpty(res.Permissions))
                    continue;
                var parts = res.Permissions.Split(new char[]{';'});
                var builder = new StringBuilder();
                var exp = from acl in acls
                          where acl.ResourceID==res.ResourceID
                          select acl.Permission;
                var op = exp.Count() == 0 ? 0 : exp.First();
                for (int i = 0; i < parts.Length; i++)
                {
                    if (builder.Length > 0)
                        builder.Append(";");
                    builder.AppendFormat("{0}:{1}",parts[i],
                        ((int)(Math.Pow(2,i)) & op ) >0 );
                }
                result.Add(new { ResourceID = res.ResourceID
                     ,WasCode = res.WasCode
                    ,ResourceName=res.ResourceName
                    ,Memo=res.Memo
                    ,Permissions=builder.ToString()
                });
            }
            JsonUtil.WriteJson(result);
        }


        private void GetPagedList()
        {

            var pageIndex = string.IsNullOrEmpty(Request["page"]) ? 1 : Convert.ToInt32(Request["page"]);


            var pageSize = string.IsNullOrEmpty(Request["rows"]) ? 20 : Convert.ToInt32(Request["rows"]);


            var sortColumn = Request["sortColumn"];


            var sortDirection = Request["sortDirection"];



            JsonUtil.WritePagedJson(EvmAMApp.Instance.GetRoleSrv().GetPagedList(pageIndex, pageSize, sortColumn, sortDirection));
        }


        private void GetByID()
        {

            var id = Convert.ToInt32(Request["RoleID"]);

            JsonUtil.WriteJson(EvmAMApp.Instance.GetRoleSrv().GetByID(id));
        }

        private void Add()
        {

            var userName = Request["userName"];

            var role = new Model.Role();

            IList<int> organIDList = null;
            IDictionary<int, int> acls = null;
            GetRole(role,out organIDList,out acls);

            EvmAMApp.Instance.GetRoleSrv().Add(userName, role, organIDList,acls);
            JsonUtil.WriteSuccessReponse();
        }

        private void GetRole(Model.Role obj,out IList<int> organs,out IDictionary<int,int> acls)
        {
            obj = obj == null ? new Model.Role() : obj;
            organs = new List<int>();
            acls = new Dictionary<int, int>();
            if (!string.IsNullOrEmpty(Request["RoleName"]))
                obj.RoleName = Request["RoleName"];

            if (!string.IsNullOrEmpty(Request["Memo"]))
                obj.Memo = Request["Memo"];
            if (!string.IsNullOrEmpty(Request["Organs"]))
            {
                foreach (var organ in Request["Organs"].Split(new char[] { ';' }))
                {
                    organs.Add(int.Parse(organ));
                }
            }
            else
                organs = null;

            var tvls = Request.Form.GetValues("acl");
            if (tvls == null) 
                return;
            foreach (var t in tvls)
            {
                var parts = t.Split(new char []{':'});
                var resID = int.Parse(parts[0]);
                var v = int.Parse(parts[1]);
                if (acls.ContainsKey(resID))
                {
                    acls[resID] = acls[resID] | v;
                }
                else
                {
                    acls.Add(resID, v);
                }
            }
        }

        private void Save()
        {

            var userName = Request["userName"];

            var role = EvmAMApp.Instance.GetRoleSrv().GetByID(int.Parse(Request["RoleID"]));

            IList<int> organIDList = null;
            IDictionary<int, int> acls;
            GetRole(role,out organIDList,out acls);
            EvmAMApp.Instance.GetRoleSrv().Save(userName, role, organIDList,acls);
            JsonUtil.WriteSuccessReponse();
        }

        private void DeleteByID()
        {

            var userName = Request["userName"];

            var roleID = Convert.ToInt32(Request["roleID"]);
           EvmAMApp.Instance.GetRoleSrv().DeleteByID(userName, roleID);
            JsonUtil.WriteSuccessReponse();
        }

    }
}





