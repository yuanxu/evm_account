﻿<%@ Page Language="C#" AutoEventWireup="true"  ClientIDMode="Static"  EnableViewState="false"  %>
<%Evm.AccountManagement.WebAuth.Client.WebAuthHelper.CheckPermission("Role", 1);  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <am:Style runat="server" />
    <script type="text/javascript">
        using(["messager", "dialog","datagrid"]);
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <div id="tt"></div>
    <div id='editor' style='width:460px;height:500px;display:none;overflow:visible' url='RoleEditor.aspx' title='角色信息' ></div>
    <script type="text/javascript">
        using("datagrid", function () {
            
            $("#tt").datagrid({
                url: "RoleSrvController.aspx?method=GetPagedList",
                width: 780,
                height: 480,
                pagination: true,
                rownumbers: true,
                singleSelect: true,
                pageSize: 20,
                columns: [[
                     { field: "RoleName", title: "角色名",width:120 }
                    , { field: "Memo", title: "备注" ,width:400}
                ]]
                 , toolbar: [{
                    iconCls: 'icon-add',
                    text: '新建',
                    handler: function () { onAdd(); }
                }, {
                    iconCls: 'icon-edit',
                    text: '修改',
                    handler: function () { onEdit(); }
                }, {
                    iconCls: 'icon-cut',
                    text: '删除',
                    handler: function () { onDelete(); }
                }]
            });
        });
        function onAdd()
        {
            $('#editor').editor('add');
        }
         function onEdit()
        {
            $('#editor').editor('edit',{
                gridId:'tt',
                idField:'RoleID'
            });
        }
        function onDelete()
        {
         $('#editor').editor('delete',{
                url:'RoleSrvController.aspx?method=DeleteByID',
                gridId:'tt',
                idField:'RoleID',
                nameField:'RoleName'
            });
        }
        function closeEditor(reload) {
            $('#editor').dialog('close');
            if(reload)
                $('#tt').datagrid('reload');
        }
    </script>
   </form>
</body>
</html>
