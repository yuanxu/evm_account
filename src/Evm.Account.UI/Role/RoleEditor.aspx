﻿
<%@ Page Language="C#" AutoEventWireup="true"  ClientIDMode="Static"  EnableViewState="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    Evm.AccountManagement.WebAuth.Client.WebAuthHelper.CheckPermission("Account", 1);
    var isAdd = string.IsNullOrEmpty(Request["RoleID"]);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Role编辑器</title>
  <am:Style runat="server" />
    <script type="text/javascript">
        using("messager");
    </script>
</head>

<body>
    <form id="form1"  method="post"  action='RoleSrvController.aspx?Method=<%=isAdd?"Add":"Save&RoleID="+Request["RoleID"]%>'>
    
    <p>
        <label id="RoleNameLabel" for="RoleName">角色名:</label>
        <input type="text" name="RoleName"  class="easyui-validatebox" required='true' vaildType='length[1,20]' />
    </p>
    <p>
        <label id="MemoLabel" for="Memo">备注:</label>
        <textarea id="Memo" name="Memo"  rows="3" cols="50" class="easyui-validatebox" validType="length[1,300]"></textarea>        
    </p>
    <div id="tb" class="easyui-tabs" fit="true" border="1" plain="true" style='height:150px' >
        <div title="所属机构">
            <br />
            <input type="hidden" name="Organs" id="Organs" />
            <div id="organ"></div>
        </div>
        <div title="账户">
            <br />
            <div id="accounts"></div>
        </div>
        <div title="授权权限">
            <br />
            <div id="acls"></div>

        </div>
    </div>
    <p class="toolbar">
        <a href='#' class='easyui-linkbutton' iconCls="icon-ok" onclick='return onSubmit()'>确定</a>
            <a href='#' class='easyui-linkbutton' iconCls="icon-cancel" onclick='onCancel()'>取消</a>
    </p>
    </form>
    <script type="text/javascript">
        function onSubmit() {
            var list = $("#organ").tree('getChecked');
            var organs="";
            for(var i = 0 ; i<list.length ; i++)
            {
                if (i!=0) organs +=";";
                organs+=list[i].id;
            }
            $("#Organs").val(organs);


            $('#form1').submitForm();
        }
        function onCancel() {
            parent.closeEditor();
        }
        using("tree",function(){
            var organDiv = $("#organ");
            organDiv.tree({
                url:'../Shared/Helper.aspx?method=GetOrganTree'
                ,checkbox:true
                ,cascadeCheck:false
                ,onLoadSuccess:function(node,data){
                    $.post('../Shared/Helper.aspx?method=GetOrgansByRoleID&RoleID=<%=Request["RoleID"] %>'
                        ,null,function(data){
                            if(data == null || data == undefined || data=="") return;
                            var parts = data.split(";");
                            for (var i = 0 ;i < parts.length ; i++) {
                               var node = organDiv.tree('find',parts[i]);  
                              organDiv.tree('check',node.target);  
                            }
                        });
                }
            });
        });

          <%
        if(!isAdd)
        {
         %>
         using("form",function(){
            $("#form1").form('load','RoleSrvController.aspx?method=GetByID&RoleID=<%=Request["RoleID"]+"&t="+DateTime.Now.Millisecond %>');
         });
         using("datagrid",function(){
            $('#accounts').datagrid({
                url:'../Shared/Helper.aspx?method=GetAccountsByRoleID&roleID=<%=Request["RoleID"] %>',
                 height:230,
                rownumbers: true,
                singleSelect: true   ,
                columns:[[
                {field:"UserName",title:'账户',width:80},
                {field:"Name",title:'姓名',width:80},
                {field:"Memo",title:'备注',width:300}
                ]]
            });
            
         });
         <%} %>
         using("datagrid",function(){
            $('#acls').datagrid({
                url:'RoleSrvController.aspx?method=GetAclsByRoleID&roleID=<%=Request["RoleID"] %>',
                 height:230,
                rownumbers: true,
                singleSelect: true   ,
                columns:[[
                {field:"WasCode",title:"Was",width:80},
                {field:"ResourceName",title:'资源',width:80},
                {field:"Permissions",title:'权限',width:280,formatter:function(value,row,index){
                    var parts = value.split(";");
                    var result = "";
                    for(var i = 0 ;i < parts.length ;i ++){
                        var kv = parts[i].split(":");
                        var v =row.ResourceID+":"+ Math.pow(2,i);
                        var checked= ''; if(kv[1]=='True') checked='checked="checked"';
                        result+="<span><input type='checkbox' name='acl' class='rights' value='"+v+"' "+checked+" />"+
                            kv[0]+"</span> ";
                    }
                    return result;
                }},
                {field:"Memo",title:'备注',width:100}
                ]]
            });
        });
    </script>
</body>
</html>





