﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Evm.AccountManagement.WebAuth;
using Evm.AccountManagement.WebAuth.Server;

namespace Evm.AccountManagement.UI.WebAuth
{
    public static class WebKdcConfig
    {
         static WebKdcConfig()
        {
            Secret = Guid.NewGuid().ToByteArray();
            SessionExpireTime = 5;
            TokenExpireTime = 5;

            
            TokenStore = new MemoryTokenStore();
        }
        /// <summary>
        /// WebKdc 私钥
        /// </summary>
        public static byte[] Secret { get; set; }

        /// <summary>
        /// Session过期时间，单位分钟。默认5分钟
        /// </summary>
        public static int SessionExpireTime { get; set; }

        /// <summary>
        /// Token过期时间，单位分钟。默认5分钟
        /// </summary>
        public static int TokenExpireTime { get; set; }

     

        public static ITokenStore TokenStore { get; set; }
    }
}