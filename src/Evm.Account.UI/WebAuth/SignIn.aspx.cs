﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Evm.AccountManagement.Service;
using Evm.Rtl;
using Evm.AccountManagement.WebAuth.Tokens;
using System.Text;
using Evm.AccountManagement.WebAuth;
using Evm.Rtl.Web;
namespace Evm.AccountManagement.UI.WebAuth
{
    /// <summary>
    /// 登录界面
    /// URL参数:ST(webkdc-service token),RT(RequestToken,含ru)
    /// </summary>
    public partial class SignIn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["method"]=="DoSignIn")
            {
                DoSignIn();
                Response.End();
            }
        }

        private void DoSignIn()
        {
            var userName = Request["UserName"];
            var password = Request["Password"];
            var acc=EvmAMApp.Instance.GetAccountSrv().GetByUserName(userName);
            if (acc == null || acc.Password != password) //TODO:密码加密存放
            {
                JsonUtil.WriteJson(new InvokeAuthenticationError());
                
                return;
            }
            WebKdcServiceToken st = new WebKdcServiceToken(WebKdcConfig.Secret);
            st.DecodeFrom(Request["ST"].Replace(" ","+"));

            var rtk = new RequestToken(st.SessionKey);
            rtk.DecodeFrom(Request["RT"].Replace(" ", "+"));

            //登录成功。写入webkdc-proxy token
            WebKdcProxyToken tk = new WebKdcProxyToken(WebKdcConfig.Secret);
            tk.Subject = userName;
            tk.CreationTime = DateTime.Now;
            tk.ExpirationTime = DateTime.Now.AddMinutes(WebKdcConfig.TokenExpireTime);
           

            tk.ProxySubject = st.Subject;
            var wasCode = st.Subject.Substring("krb5:".Length);
            var list = EvmAMApp.Instance.GetAclSrv().GetListByUserNameAndWasCode(userName, wasCode);
            //格式:rescode:permission;
            var builder = new StringBuilder();
            foreach (var item in list)
            {
                if (builder.Length > 0)
                    builder.Append(";");
                builder.AppendFormat("{0}:{1}", item.ResourceCode, item.ObjectPermission);
            }
            tk.ProxyData = Encoding.UTF8.GetBytes(builder.ToString());

            CookieUtils.SetCookie(WebAuthCookieName.webauth_wpt_webkdc,tk.Encode());

            var it = new IdToken(st.SessionKey);
            it.Subject=userName;
            it.Acls=builder.ToString();
            it.SubjectAuthenticatorType="webkdc";
            
            //转到成功提示页面
            JsonUtil.WriteJson(new InvokeResult(true, 0, "", string.Format("Confirmation.aspx?RU={0}&IT={1}", rtk.ReturnUrl, it.Encode())));
            
        }
    }
}