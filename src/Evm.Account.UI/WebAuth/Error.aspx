﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="Evm.AccountManagement.UI.WebAuth.Error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>错误</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h3>发生错误！</h3>
        <p>因为<asp:Label ID="lbErr" runat="server"></asp:Label>，您的请求无法被处理，可能是因为等待时间过程或者数据已被篡改。请点击地址重新发起请求：
        <asp:HyperLink runat="server" ID="hl"></asp:HyperLink></p>
    </div>
    </form>
</body>
</html>
