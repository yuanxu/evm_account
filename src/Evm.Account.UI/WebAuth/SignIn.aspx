﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignIn.aspx.cs" Inherits="Evm.AccountManagement.UI.WebAuth.SignIn" ViewStateMode="Disabled" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <am:Style runat="server" />
    <script type="text/javascript">
        using(["form", "validate"]);
    </script>
</head>
<body>
    <form id="form1" method="post" action="SignIn.aspx?method=DoSignIn">
    <div region="north" border="false" style="background:#666;text-align:center">        <p><br />
        Evm Account Management System v1.0
        <br />
        </p>
	</div>

    <fieldset style="width:600px">
        <legend>登录到EvmAM.</legend>
    
        <input name="RT" type="hidden" value="<%=Request["RT"]%>" />
        <input name="ST" type="hidden" value="<%=Request["ST"]%>" />
        <p> <label for="UserName">用户名：</label>
            <input id="UserName" name="UserName"  type="text" class="easyui-validatebox" required='true' vaildType='length[1,20]'  />
        </p>
    
        <p>
            <label for="Password">密码：</label>
            <input id="Password" name="Password" type="password" class="easyui-validatebox" required='true' vaildType='password' />
        </p>
    
        <p>
            <span id="error" style="color:red;height:25px"></span>
        </p>
        <p class='toolbar'>
            <input  type="button" value="登录" onclick="return onSubmit()"  />
            <input type="button" value="取消" onclick='return onCancel()' />
        </p>
    </fieldset>
    </form>
    <script type="text/javascript">
        function onSubmit() {
            $('#form1').form('submit', {
                onSubmit: function () {
                    return $(this).form('validate');
                }
                , success: function (data) {
                    var jo = eval("(" + data + ")");
                    if (jo.IsSucceed)
                        window.location.href = jo.Data;
                    else
                        $('#error').text("验证失败！用户名或密码错误.");

                }
            });

        }

        function onCancel() {
            window.close();
        }
    </script>
</body>
</html>
