﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Evm.AccountManagement.WebAuth;
using Evm.AccountManagement.WebAuth.Tokens;

namespace Evm.AccountManagement.UI.WebAuth
{
    /// <summary>
    /// 入口参数:RU=&IT=
    /// </summary>
    public partial class Confirmation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Request["RU"]))
            {
                Response.Write("非法访问！");
            }
            else
            {
                var url = Server.UrlDecode(Request["RU"]);
                lbJump.Text = string.Format("{0}{1}IT={2}", url, url.IndexOf("?") >= 0 ? "&" : "?", Request["IT"]);
                lbJump.NavigateUrl = lbJump.Text;
            }
        }
    }
}