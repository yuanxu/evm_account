﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Evm.AccountManagement.UI.WebAuth
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            switch(Request["err"])
            {
                case "TokenInValid":
                    lbErr.Text = "令牌错误";
                    break;
                case "TokenExpired":
                    lbErr.Text = "请求已过期";
                    break;
            }
            hl.Text = Request["RT"];
            hl.NavigateUrl = hl.Text;
        }
    }
}