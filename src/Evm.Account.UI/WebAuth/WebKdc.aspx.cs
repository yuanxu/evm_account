﻿/*
 *  WebAuth KDC 实现
 */ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Evm.AccountManagement.WebAuth;
using Evm.AccountManagement.WebAuth.Tokens;
using Evm.AccountManagement.WebAuth.XmlCommands;
using Evm.AccountManagement.Service;
using System.Text;
using System.Security.Cryptography;

namespace Evm.AccountManagement.UI.WebAuth
{
    public partial class WebKdc : System.Web.UI.Page
    {
        private const string WEBAUTH_CONTENTTYPE = "text/xml";

        protected void Page_Load(object sender, EventArgs e)
        {
           
            //重定向。从UA过来请求验证，处理完成后需要返回id token
            if (Request.HttpMethod == "GET")
            {
                ProcessRedirect();
                return;
            }
            //检查收到的xml command

            if (Request.ContentType != WEBAUTH_CONTENTTYPE)
            {
                Response.StatusCode = 406;
                Response.StatusDescription = "不支持的Content-Type";
                return;
            }
            var content = "";
            using (StreamReader reader = new StreamReader(Request.InputStream))
            {
                content = reader.ReadToEnd();
            }

            var cmd = content.Substring(1, content.IndexOf(">") - 1);
            switch (cmd)
            {
                case "getTokensRequest":
                    ProcessGetTokens( content);
                    break;
                case "requestTokenRequest":
                    ProcessRequestToken( content);
                    break;
                case "webkdcProxyTokenRequest":
                    ProcessProxyToken( content);
                    break;
            }
        }

        private string GetUrlBase64String(string paramName)
        {
            return Request[paramName].Replace(" ", "+");
        }

        /// <summary>
        /// 重定向，url中必须包含RT和ST
        /// </summary>
        private void ProcessRedirect()
        {
            if (string.IsNullOrEmpty(Request["RT"]) || string.IsNullOrEmpty(Request["ST"]))
            {
                //Response.Status = "400 Bad Request";
                Response.StatusCode = 400;
                return;
            }
           
            var st = new WebKdcServiceToken(WebKdcConfig.Secret);
            var sts = GetUrlBase64String("ST");
            try
            {
                st.DecodeFrom(sts);
            }
            catch
            {
                Response.Redirect("Error.aspx?err=TokenInValid&RU=" + Request.UrlReferrer == null ? "" : Request.UrlReferrer.ToString(), true);
                return;
            }
            var rt = new RequestToken(st.SessionKey);
            var rts = GetUrlBase64String("RT");
            try
            {
                rt.DecodeFrom(rts);
            }
            catch {
                Response.Redirect("Error.aspx?err=TokenInValid&RU=" + Request.UrlReferrer == null ? "" : Request.UrlReferrer.ToString(), true);
                return;
            }
            //验证rt和st是否已过期
            if( !rt.IsValid() )
            {
                Response.Redirect("Error.aspx?err=TokenInValid&RU="+Request.UrlReferrer==null?"":Request.UrlReferrer.ToString(),true);
                return;
            }
            else if(rt.IsExpired())
            {
                Response.Redirect("Error.aspx?err=TokenExpired&RU="+ rt.ReturnUrl ,true);
                return;
            }
            if (!st.IsValid())
            {
                Response.Redirect("Error.aspx?err=TokenInValid&RU=" + Request.UrlReferrer == null ? "" : Request.UrlReferrer.ToString(), true);
                return;
            }
            else if (rt.IsExpired())
            {
                Response.Redirect("Error.aspx?err=TokenExpired&RU=" + rt.ReturnUrl, true);
                return;
            }

           //检查cookie中是否有webkdc-proxy  
            if (CookieUtils.IsExist(WebAuthCookieName.webauth_wpt_webkdc))
            {
                //可单点登录，返回登录成功界面
                var wpt = new WebKdcProxyToken(WebKdcConfig.Secret);
                wpt.DecodeFrom(CookieUtils.GetCookie(WebAuthCookieName.webauth_wpt_webkdc));
                if (wpt.IsExpired() || !wpt.IsValid())
                {
                    Response.Redirect(string.Format("SignIn.aspx?RT={0}&ST={1}",rts,sts));
                    return;
                }
                //检查st和wpt中的subject是否匹配，不匹配需要设置新的service-proxy token
                if (st.Subject != wpt.ProxySubject)
                {
                    var nwpt = (WebKdcProxyToken)wpt.Clone();
                    nwpt.CreationTime = DateTime.Now;
                    nwpt.ExpirationTime = DateTime.Now.AddMinutes(WebKdcConfig.TokenExpireTime);
                    CookieUtils.SetCookie( WebAuthCookieName.webauth_wpt_webkdc,nwpt.Encode());
                }
                //设置id token
                IdToken it = GetIdToken(st, wpt);
                Server.Transfer(string.Format("Confirmation.aspx?&RU={0}&IT={1}",Server.UrlEncode(rt.ReturnUrl),Server.UrlEncode(it.Encode())));
            }
            else //转到登录界面
            {
                Response.Redirect(string.Format("SignIn.aspx?RT={0}&ST={1}", rts,sts));
            }
        }

        /// <summary>
        /// 构建ID令牌
        /// </summary>
        /// <param name="st">会话令牌</param>
        /// <param name="wpt">代理令牌</param>
        /// <returns></returns>
        private static IdToken GetIdToken(WebKdcServiceToken st, WebKdcProxyToken wpt)
        {
            IdToken it = new IdToken(st.SessionKey);
            it.Subject = wpt.Subject;
            var builder = new StringBuilder();
            foreach (var item in EvmAMApp.Instance.GetAclSrv().GetListByUserNameAndWasCode(it.Subject, wpt.ProxySubject.Substring("krb5:".Length)))
            {
                if (builder.Length > 0)
                    builder.Append(";");
                builder.Append(string.Format("{0}:{1}", item.ResourceCode, item.ObjectPermission));
            }
            it.Acls = builder.ToString();
            
            builder.Clear();
            var accID = EvmAMApp.Instance.GetAccountSrv().GetByUserName(it.Subject).AccountID;
            foreach (var item in EvmAMApp.Instance.GetHoldPostSrv().GetListByAccountID(accID))
            {
                if (builder.Length > 0)
                    builder.Append(";");
                builder.Append(string.Format("{0}@{1}:{2}", item.Post.Role.RoleName,item.Post.Organ.OrganCode, item.IsConcurrentPost));
            }
            return it;
        }

        private void ProcessProxyToken(string content)
        {
            throw new NotImplementedException();
        }

        private void ProcessGetTokens(string content)
        {
            var req = GetTokensRequest.Deserialize<GetTokensRequest>(content);
            if (req.Tokens == null || req.Tokens.Length == 0)
            {
                //请求不完整
                Response.Write(new RequestInvalidError().GetErrorResponseString());
                return;
            }
            if (req.RequesterCredential.Type == "krb5")//request webkdc-service & session key
            {
                #region request webkdc-service & session key
                
                if (req.Tokens.Length > 1 || req.Tokens[0].Type != "service")
                {
                    //请求不完整或有错误
                    Response.Write(new RequestInvalidError().GetErrorResponseString());
                    return;
                }
                //解码{base64-krb5-mk-req-data}。内中存放的格式为：base64({hmac}{nonce}{wasCode}{padding})
                byte[] buf;
                try
                {
                    buf = Convert.FromBase64String(req.RequesterCredential.Token);
                }
                catch (Exception)
                {
                    Response.Write(new WasPrincipleCorruptError().GetErrorResponseString());
                    return;
                }
                var padLen = buf[buf.Length - 1];
                var wasCode = Encoding.UTF8.GetString(buf, 36, buf.Length - 36 - padLen);//36=len(hmac)+lan(nonce)=20+16
                var was = EvmAMApp.Instance.GetWasSrv().GetByCode(wasCode);
                if (was == null)
                {
                    Response.Write(new Krb5RequesterCrendentialBasError().GetErrorResponseString());
                    return;
                }
                //验证Was是否正确
                HMACSHA1 hmac = new HMACSHA1(Encoding.UTF8.GetBytes(was.Secret));
                var computedHmac = hmac.ComputeHash(buf, 20, buf.Length - 20);

                for (int i = 0; i < computedHmac.Length; i++)
                {
                    if (computedHmac[i] != buf[i])
                    {
                        Response.Write(new Krb5RequesterCrendentialBasError().GetErrorResponseString());
                        return;
                    }
                }

                //was验证成功
                WebKdcServiceToken token = new WebKdcServiceToken(WebKdcConfig.Secret);
                token.GenerateSessionKey();
                token.Subject = "krb5:"+wasCode;
                //保存SessionKey
               // token.KeyHint = WebKdcConfig.SessionKeyStore.Save(token.SessionKey, WebKdcConfig.SessionExpireTime);
                var resp = new GetTokensResponse();
                var tokenData = token.Encode();
                //保存token 
                WebKdcConfig.TokenStore.Save(tokenData, WebKdcConfig.TokenExpireTime);
                resp.Tokens = new XmlToken[]
                {
                    new XmlToken()
                    {
                        SessionKey=Convert.ToBase64String(token.SessionKey) //session key
                        , TokenData= tokenData//webkec-service token
                        , Expires=DateTime.Now.AddMinutes(WebKdcConfig.SessionExpireTime) // session key expire  time
                    }
                };

                Response.Write(resp.GetString());

                #endregion
            }
            else if (req.RequesterCredential.Token == "service")//request id、proxy token
            {
                //检查request token是否正确
                if (!WebKdcConfig.TokenStore.IsValid(req.RequestToken))
                {
                    Response.Write(new RequestTokenStaleError().GetErrorResponseString());
                    return;
                }
                var resp = new GetTokensResponse();

                foreach (var tk in req.Tokens)
                {
                    switch (tk.Type)
                    {
                        case "id":
                            break;
                        case "proxy":
                            break;
                        case "cred":
                            break;
                        default:
                            Response.Write(new RequestInvalidError().GetErrorResponseString());
                            return;
                    }
                }
            }
        }

        private void ProcessRequestToken(string content)
        {

            throw new NotImplementedException();
        }
    }
}