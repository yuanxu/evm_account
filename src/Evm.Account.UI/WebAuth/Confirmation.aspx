﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Confirmation.aspx.cs" Inherits="Evm.AccountManagement.UI.WebAuth.Confirmation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1">
    <div region="north" border="false" style="background:#666;text-align:center">        <p><br />
        Evm Account Management System v1.0
        <br />
        </p>
	</div>
    <div>
    <h3>登录成功！</h3>
    <p>恭喜您已经成功登录！如果您的浏览器在3秒内没有正常跳转，请点击下面的链接：</p>
    <p><asp:HyperLink ID="lbJump" runat="server" ></asp:HyperLink></p>
    
    </div>
    </form>
</body>
</html>
