﻿<%@ Page Language="C#" AutoEventWireup="true"  ClientIDMode="Static"  EnableViewState="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    Evm.AccountManagement.WebAuth.Client.WebAuthHelper.CheckPermission("Account",1);
    var isAdd = string.IsNullOrEmpty(Request["AccountID"]);
    var AccountID = isAdd?0:Convert.ToInt32( Request["AccountID"]);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head >
    <title>Account编辑器</title>
    <am:Style runat="server" />
    <script type="text/javascript">
        using("messager");
        using("combotree");
        using("combobox");
        using("datagrid");
        using("form");
    </script>
</head>

<body>
    <form id="form1"  method="post"  action='AccountSrvController.aspx?Method=<%=isAdd?"Add":"Save&AccountID="+Request["AccountID"]%>'  >
    <div id="tt" class="easyui-tabs" fit="true" border="false" plain="true" style="height:370px" onclick="onTabChanged()" >
		<div title="基本信息">
            <p>
                <label for="Name">姓名:</label>
                <input class="easyui-validatebox" type="text" name="Name" required='true' validType="length[2,10]" />
             </p>
             <p>
                <label for="OrganID">部门:</label>
                <select name="OrganID" id="OrganID" >     </select>
            </p>
            <p>
                <label for="UserName">用户名:</label>
                <input class="easyui-validatebox" type="text" name="UserName"  required='true'  validType="length[2,20]" />
            </p>
            <p>
                <label for="Password">密码：</label>
                <input class="easyui-validatebox" type="text" name="Password"  required='true'  validType="length[3,10]" />
            </p>
            <p>
                <label for="Email">Email:</label>
                <input class="easyui-validatebox" type="text" name="Email" validType="email"  validType="length[0,100]" />
            </p>
            <p>
                <label for="OfficePhone">办公电话:</label>
                <input class="easyui-validatebox" type="text" name="OfficePhone"   maxlength="15" />
            </p>
            <p>
                <label for="HomePhone">家庭电话:</label>
                <input class="easyui-validatebox" type="text" name="HomePhone"   maxlength="15" />
            </p>
            <p>
                <label for="Mobile">手机:</label>
                <input class="easyui-validatebox" type="text" name="Mobile"   maxlength="11" />
            </p>
            <p>
                <label for="IsLockedOut">是否锁定:</label>
                <input class="easyui-validatebox" type="checkbox"  name="IsLockedOut"   />
            </p>

    </div>
    <div title='上级'>
    <br />
     <p><label><input type="radio" name="Manager" value='0' checked="checked" />没有上级</label>
                </p>
                <p>
                    <label >
                        <input type="radio" name="Manager" value="1" />指定人：</label>
                    <select name="ManagerID" id="ManagerID" class='easyui-combotree'></select>
                </p>
                <p>
                    <label>
                        <input type="radio" name="Manager" value="2" />指定岗位：
                    </label>
                    <select name="ManagerRoleID" id="ManagerRoleID" class='easyui-combotree'></select>
                </p>
    </div>
    <div title='角色' >
        <br />
            <div id='OwnerRoles'></div>
        <br />
            <div id='OtherRoles'></div>
            <input type="hidden" name="Posts" id="Posts" />
    </div>
    
    </div>
    <div class='toolbar' style='text-align:right;'>
        <a href='#' class='easyui-linkbutton' iconCls="icon-ok" onclick='return onSubmit()'>确定</a>
        <a href='#' class='easyui-linkbutton' iconCls="icon-cancel" onclick='onCancel()'>取消</a>
    </div>
    </form>
    <script type="text/javascript">
        var lastOrganID,lastOrganID2;

        function onTabChanged() {
            var tab = $('#tt').tabs('getSelected');
            switch (tab.panel('options').title) {
                case "上级":
                    var v = $('#OrganID').combotree('getValue');
                    if (v == "") v = 0;
                    if (v != lastOrganID) {
                        lastOrganID = v;
                        $('#ManagerID').combobox({ url: '../Shared/Helper.aspx?method=GetOrganUserList&organID=' + v+"&AccountID=<%=AccountID %>", valueField: 'id', text: 'text',editable:false });
                        $('#ManagerRoleID').combobox({ url: '../Shared/Helper.aspx?method=GetOrganRoleList&organID=' + v, valueField: 'id', text: 'text' ,editable:false});
                    }
                    break;
                case "角色":
                    var v = $('#OrganID').combotree('getValue');
                    if (v == "") v = 0;
                    if (v != lastOrganID2) {
                        lastOrganID2 = v;
                        var selected = new Array();
                        $('#OwnerRoles').datagrid({
                            title: '本部门角色',
                            url: '../Shared/Helper.aspx?method=GetOrganRoleData&organID=' + v + "&AccountID=<%=AccountID %>",
                            width: 420,
                            height: 100,
                            columns: [[
                            { field: 'PostID', checkbox: true, wdith: 40
                             , styler: function (value, rowData, rowIndex) {
                                  if(rowData.IsHold)
                                  {
                                    selected.push(rowIndex);
                                   }
                                  return "";
                             }
                            },
                            { field: 'RoleName', title: '角色', width: 80 },
                            { field: 'Memo', title: '备注', width: 120} ]] ,
                            onLoadSuccess:function(){
                                for (var i = 0; i < selected.length ;i++) {
                                    var idx=selected[i];
                                    $('#OwnerRoles').datagrid('selectRow',idx);
                                }
                            }
                        });
                        
                        selected = new Array();
                        $('#OtherRoles').datagrid({
                            title: '兼职其他部门角色',
                            url: '../Shared/Helper.aspx?method=GetOtherOrganRoleData&organID=' + v + "&AccountID=<%=AccountID %>",
                            width: 420,
                            height: 200,
                            columns: [[
                            { field: 'PostID', checkbox: true, wdith: 40
                                 , styler: function (value, rowData, rowIndex) {
                                  if(rowData.IsHold)
                                  {
                                    selected.push(rowIndex);
                                   }
                                  return "";
                             }
                            },
                            { field: 'RoleName', title: '角色', width: 80 },
                            { field: 'OrganName', title: '部门', width: 180 },
                            { field: 'Memo', title: '备注', width: 100}]],
                            onLoadSuccess:function(){
                                for (var i = 0; i < selected.length ;i++) {
                                    var idx=selected[i];
                                    $('#OtherRoles').datagrid('selectRow',idx);
                                }
                            }
                        });
                    }
                    break;
                default:
                    
                    break;
            }
        }
        function onSubmit() {
            var posts = "";
            try{ //忽略角色中可能的错误
                var rows =  $('#OwnerRoles').datagrid('getSelections');
                for (var i = 0; i < rows.length; i++ ) {
                    posts += ";" + rows[i].PostID + ":false";
                }
                rows =  $('#OtherRoles').datagrid('getSelections');
                for (var i = 0; i < rows.length; i++ ) {
                    posts +=";"+rows[i].PostID+":true";
                }
                if (posts.length > 0)
                    $('#Posts').val(posts.substr(1));
            }catch(e){}

            $('#form1').submitForm();
        }
        function onCancel() {
            parent.closeEditor();
        }
            using("combotree",function(){
                //初始化部门/组织机构
                $('#OrganID').combotree(
                    {url:'../Shared/Helper.aspx?method=GetOrganTree',
                    textField:'text',
                    valueField:'id',
                    editable:false
            
        <%if(!isAdd)
        { %>
        ,onLoadSuccess:function(){ //组织机构加载后，加载需要编辑的数据
            using("form",function () {
                $('#form1').form(
                    { onLoadSuccess:function(data){//数据加载完成后，初始化相应的几个combobx字段
                        lastOrganID = data.OrganID;
                        using("combotree",function(){$('#OrganID').combotree('setValue',data.OrganID);});
                        if(data.ManagerID!=0)
                        {
                            $("input[name='Manager']")[1].checked=true;
                            using("combobox",function(){ $('#ManagerID').combobox({
                                url: '../Shared/Helper.aspx?method=GetOrganUserList&organID=' + data.OrganID+"&AccountID=<%=AccountID %>"
                                , valueField: 'id'
                                , text: 'text'
                                ,editable:false 
                                ,value:data.ManagerID}); 
                                });
                        }
                        else if(data.ManagerRoleID!=0)
                        {
                            $("input[name='Manager']")[2].checked=true;
                            using("combobox",function(){ $('#ManagerRoleID').combobox({
                                url: '../Shared/Helper.aspx?method=GetOrganRoleList&organID=' + data.OrganID
                                , valueField: 'id'
                                , text: 'text' 
                                ,editable:false
                                ,value:data.ManagerRoleID}); 
                            });
                        }
                    }} 
                ).form('load','AccountSrvController.aspx?method=GetByID&AccountID=<%=Request["AccountID"]+"&t="+DateTime.Now.Millisecond%>' );
            });
        }
        <%}%>
        });//load combotree
        });//using combotre
    </script>
</body>
</html>





