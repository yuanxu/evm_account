﻿<%@ Page Language="C#" AutoEventWireup="true"  ClientIDMode="Static"  EnableViewState="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%Evm.AccountManagement.WebAuth.Client.WebAuthHelper.CheckPermission("Account",1); %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <am:Style runat="server" />
    <script type="text/javascript">
        using(["messager", "datagrid", 'dialog']);
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <div id="tt"></div>
    <div id='editor' style='width:450px;height:500px;display:none;overflow:visible' url="AccountEditor.aspx" title='账户信息' >
    </div>
    <div id='searchBox' buttons="#dlg-buttons" style="display:none;padding:5px 10px">
        <div>
            <p style="padding:10px 5px"><label>请输入姓名：</label>
                <input id="key" type="text"  />
            </p>
        </div>
        <div id="dlg-buttons"> 
                <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="onFilter()">确定</a>  
                <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#searchBox').dialog('close')">取消</a>  
        </div>
    </div>
    <script type="text/javascript">
        using("datagrid", function () {
            $("#tt").datagrid({
                url: "AccountSrvController.aspx?method=GetPagedList",
                width: 780,
                height: '480',
                pagination: true,
                rownumbers: true,
                singleSelect: true,
                pageSize: 20,
                columns: [[
                    { field: "UserName", title: "用户名", width: 80 }
                    , { field: "Name", title: "姓名", width: 70 }
                    , { field: "Organ", title: "所在部门", width: 80/*, formatter: function (row) { return row.Organ.Name; } */ }
                    , { field: "Email", title: "Email", width: 160 }
                    , { field: "OfficePhone", title: "办公电话", width: 80 }
                    , { field: "HomePhone", title: "家庭电话", width: 80 }
                    , { field: "Mobile", title: "手机", width: 80 }
                    , { field: "IsLockedOut", title: "是否锁定", width: 60 }
                    , { field: "LastLoginTime", title: "最后登录时间", width: 85 }
                    , { field: "LastLoginIP", title: "最后登录IP", width: 85 }
                ]]
                , toolbar: [{
                    iconCls: 'icon-add',
                    text: '新账户',
                    handler: function () { onAdd(); }
                }, {
                    iconCls: 'icon-edit',
                    text: '修改',
                    handler: function () { onEdit(); }
                }, {
                    iconCls: 'icon-cut',
                    text: '删除',
                    handler: function () { onDelete(); }
                }, {
                    iconCls: 'icon-search',
                    text: '搜索',
                    handler: function () { onSearch(); }
                }
                              ]
            });
        });
        function onAdd() {
            $('#editor').editor('add');
        }
         function onEdit() {
             $('#editor').editor('edit', {
                 gridId: 'tt'
                 ,idField:'AccountID'
             });
            
        }
        function onDelete() {
            $('#editor').editor('delete', {
                url: 'AccountSrvController.aspx?method=DeleteByID'
                , gridId: 'tt'
                , idField: 'AccountID'
                ,nameField:'Name'
            });
        }
        function onSearch() {
            $('#searchBox').css('display', 'block').dialog({
                title:'搜索',
                modal: true
            });
        }
        function onFilter() {
            $('#searchBox').dialog('close');
            $('#tt').datagrid('load', { key:$('#key').val() });
        }
        function closeEditor(reload) {
            $('#editor').dialog('close');
            if (reload) {
                $('#tt').datagrid('reload');
            }
        }
    </script>
        
   </form>
   </body>
</html>
