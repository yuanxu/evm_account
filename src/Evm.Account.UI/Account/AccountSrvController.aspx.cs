﻿
//
//  AccountSrv 控制器代码
//  由 Evm Aspx编译器生成于2011/9/3 11:36:44。
//  警告：请不要修改本文件，以防重新生成时被覆盖
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Evm.Rtl;
using Evm.Rtl.Web;
using System.Web.UI;
using Evm.AccountManagement.Model;
using Evm.AccountManagement.Service;
using Evm.AccountManagement.WebAuth.Client;

namespace Evm.AccountManagement.UI.Account
{
    [WebAuth(ResourceCode= "Account",Permission=1)]
     public partial class AccountSrvController:WebAuthProtectedPage
     {
        //Page_Load 方法，调度
         protected void Page_Load(object sender, EventArgs e)
         {
             try
             {
                 switch (Request["method"])
                 {
                     case "Add":
                         Add();
                         break;
                     case "GetByID":
                         GetByID();
                         break;
                     case "Save":
                         Save();
                         break;
                     case "DeleteByID":
                         DeleteByID();
                         break;
                     case "GetPagedList":
                         GetPagedList();
                         break;
                     case "Lock":
                         Lock();
                         break;
                     case "UnLock":
                         UnLock();
                         break;
                 }
             }
             catch (EvmRtException err)
             {
                 JsonUtil.WriteJson(err);
             }
             catch (Exception err)
             {
                 JsonUtil.WriteJson(err);
             }
             finally
             {
                 Response.End();
             }
         }


         private void Add()
         {
             var userName = Request["userName"];
             var acc = new Model.Account();
             IList<HoldPost> posts = null;
             GetAccount(acc, ref posts);
             EvmAMApp.Instance.GetAccountSrv().Add(userName, acc, posts);
             JsonUtil.WriteJson(new InvokeResult());
         }
       
         #region 获取对象

         private void GetAccount(Model.Account obj, ref IList<HoldPost> holdPosts)
         {
             obj = obj == null ? new Model.Account() : obj;
             holdPosts = holdPosts == null ? new List<HoldPost>() : holdPosts;

             if (!string.IsNullOrEmpty(Request["OrganID"]))
                 obj.OrganID = Convert.ToInt32(Request["OrganID"]);


             if (!string.IsNullOrEmpty(Request["UserName"]))
                 obj.UserName = Request["UserName"];


             if (!string.IsNullOrEmpty(Request["Name"]))
                 obj.Name = Request["Name"];



             if (!string.IsNullOrEmpty(Request["Email"]))
                 obj.Email = Request["Email"];


             if (!string.IsNullOrEmpty(Request["OfficePhone"]))
                 obj.OfficePhone = Request["OfficePhone"];


             if (!string.IsNullOrEmpty(Request["HomePhone"]))
                 obj.HomePhone = Request["HomePhone"];


             if (!string.IsNullOrEmpty(Request["Mobile"]))
                 obj.Mobile = Request["Mobile"];


             if (!string.IsNullOrEmpty(Request["IsLockedOut"]))
                 obj.IsLockedOut = Request["IsLockedOut"] == "on";


             if (!string.IsNullOrEmpty(Request["LastLoginTime"]))
                 obj.LastLoginTime = Convert.ToDateTime(Request["LastLoginTime"]);


             if (!string.IsNullOrEmpty(Request["LastLoginIP"]))
                 obj.LastLoginIP = Request["LastLoginIP"];

             if (!string.IsNullOrEmpty(Request["Password"]))
                 obj.Password = Request["Password"];

             if (string.IsNullOrEmpty(Request["Manager"]) || Request["Manager"] == "0")
             {
                 obj.ManagerID = 0;
                 obj.ManagerRoleID = 0;
             }
             else if (Request["Manager"] == "1")
             {
                 if (!string.IsNullOrEmpty(Request["ManagerID"]))
                     obj.ManagerID = Convert.ToInt32(Request["ManagerID"]);
             }
             else if (Request["Manager"] == "2")
             {
                 if (!string.IsNullOrEmpty(Request["ManagerRoleID"]))
                     obj.ManagerRoleID = Convert.ToInt32(Request["ManagerRoleID"]);
             }
             if (!string.IsNullOrEmpty(Request["Posts"]))
             {
                 foreach (var kv in Request["Posts"].Split(new char[] { ';' }))
                 {
                     var parts = kv.Split(new char[] { ':' });
                     holdPosts.Add(new HoldPost() { PostID = int.Parse(parts[0]), AccountID = obj.AccountID, IsConcurrentPost = Convert.ToBoolean(parts[1]) });
                 }
             }
             else
             {
                 holdPosts = null;
             }
         }

         #endregion
         
         private void GetByID()
        {
            var id = Convert.ToInt32(Request["AccountID"]) ;
            JsonUtil.WriteJson(EvmAMApp.Instance.GetAccountSrv().GetByID(id));
        }

         private void Save()
         {
             var userName = Request["userName"];
             var acc = EvmAMApp.Instance.GetAccountSrv().GetByID(int.Parse(Request["AccountID"]));
             IList<Model.HoldPost> holdPosts = null;
             GetAccount(acc, ref holdPosts);
             EvmAMApp.Instance.GetAccountSrv().Save(userName, acc, holdPosts);
             JsonUtil.WriteJson(new InvokeResult());
         }

         private void DeleteByID()
         {
             var userName = Request["userName"];
             var AccountID = Convert.ToInt32(Request["AccountID"]);
             EvmAMApp.Instance.GetAccountSrv().DeleteByID(userName, AccountID);
             JsonUtil.WriteJson(new InvokeResult());
         }



         private void GetPagedList()
         {
             var pageIndex = string.IsNullOrEmpty(Request["page"]) ? 1 : Convert.ToInt32(Request["page"]);
             var pageSize = string.IsNullOrEmpty(Request["rows"]) ? 20 : Convert.ToInt32(Request["rows"]);
             var key = Request["key"];
             JsonUtil.WriteJson(EvmAMApp.Instance.GetAccountSrv().GetPagedList(key, pageIndex, pageSize));
         }
        
        
        
        private void Lock()
        {
                        
               var userName = Request["userName"] ;
            
            
               var toLockID = Convert.ToInt32(Request["toLockID"]) ;
            


                 EvmAMApp.Instance.GetAccountSrv().Lock(userName,toLockID) ;
        }
        
        
        
        private void UnLock()
        {
                        
               var userName = Request["userName"] ;
            
            
               var toLockID = Convert.ToInt32(Request["toLockID"]) ;
            


                 EvmAMApp.Instance.GetAccountSrv().UnLock(userName,toLockID) ;
        }
        
        
        
     
        
        
     }
}





