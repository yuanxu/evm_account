﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="Evm.AccountManagement.UI.Welcome" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome</title>
    <am:Style runat="server" />
    <style type="text/css">
        p
        {
            height:25px;
            line-height:25px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
      <p>欢迎使用Evm AM账户管理系统！</p>
      <p>
        EvmAM提供基于角色和企业组织架构的权限管理方案，并且内置支持WebAuth协议的点单登录(SSO)模块。开发人员可以将本系统作为具体业务应用系统的基础，可直接在本系统的页面架构之上增加业务系统所需功能；当然也可以仅是后台服务之间的集成而不包括界面端。
      </p>
      <p>
        虽然本系统基于采用Evm开发，可作为Evm应用开发的参考。但不强制要求业务系统也采用Evm进行开发(虽然强烈建议如此)。
      </p>
    </form>
</body>
</html>
