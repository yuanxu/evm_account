﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Evm.Rtl;

namespace Evm.AccountManagement.UI
{
    public partial class Install : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Engine.Instance.OnMessage += new EventHandler<EngineEventArgs>(Instance_OnMessage);
                Engine.Instance.UploadApp(@"D:\Codes\Evm.Account\src\Evm.Account.Core\app.xml");
               
                Engine.Instance.Load("Account",true);   
            }
        }

        void Instance_OnMessage(object sender, EngineEventArgs e)
        {
            Response.Write(e.Message+"<br />");
            Response.Flush();
        }
    }
}