﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Evm.AccountManagement.WebAuth;
using System.IO;
using System.Xml;
using NUnit.Framework;
using Evm.AccountManagement.WebAuth.XmlCommands;
using Evm.AccountManagement.WebAuth.Tokens;

namespace Evm.Account.WebAuth.Test
{
    [TestFixture]
    public class TokenTest
    {
        [TestCase]
        public void RequestTokenRequestSerializeTest()
        {
            var requestToken = RequestTokenRequest.Create();
            requestToken.MessageId = "aaa";
            requestToken.RequestToken = "tk";
            requestToken.SubjectCredential.Type = "krb5";
            requestToken.RequestInfo.RemoteUser = "user";
            var result = requestToken.Serialize();

            var obj = RequestTokenRequest.Deserialize<RequestTokenRequest>(result);
            Assert.AreEqual(requestToken.MessageId, obj.MessageId);
            Assert.AreEqual(requestToken.RequestInfo.RemoteUser,obj.RequestInfo.RemoteUser);
        }

        [TestCase]
        public void WebKdcServiceTokenTest()
        {
            var key = Guid.NewGuid().ToByteArray();
            WebKdcServiceToken token = new WebKdcServiceToken(key);
            //token.ServerIdentity = "YuanXu-PC";
            //token.CreationTime = DateTime.Now;
            var encoed=token.Encode();
            WebKdcServiceToken token2 = new WebKdcServiceToken(key);
            token2.DecodeFrom(encoed);
           // Assert.AreEqual(token.ServerIdentity,token2.ServerIdentity);
            Assert.IsTrue(token2.IsValid());
        }

        [TestCase]
        public void GetTokensCommandTest()
        {
            var tc = new GetTokensRequest();
            tc.RequestToken = "rt";
            tc.RequesterCredential = new RequesterCredential() { Token = "tk", Type = "webkdc" };
            tc.SubjectCredential = new SubjectCredential() { Type="proxy", ProxyToken="ptk" };
            tc.Tokens = new Evm.AccountManagement.WebAuth.XmlCommands.XmlToken[1];
            tc.Tokens[0] = new Evm.AccountManagement.WebAuth.XmlCommands.XmlToken() { Authenticator = new Authenticator() { Type="webkdc" } , Type="kdb" , CredentialType="ct" };

            var result = tc.Serialize();

        }
    }
}
