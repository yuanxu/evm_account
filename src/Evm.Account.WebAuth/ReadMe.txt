﻿
		===============================
					Read Me
		===============================

1.概述
	本项目乃依据斯坦福大学WebAuth v3协议实现(www.stanford.edu/services/webauth/protocol.html)。
	鉴于Evm应用的场景大都是基本可信任的网络环境或者对安全性要求并不苛刻，因此本项目所采用的验证方式为Web Form/WebKdc形式，而没有采用Kerberos。
	同时根据EvmAM的设计，对WebAuth有所扩展(主要是在id,cred,app token中增加acl特性[attribute])。
2.令牌(Tokens)
	根据EvmAM的设计，并未使用WebAuth规范中的所有令牌，而是其中的一个自己。下面是本实现中用到的令牌：
	
	webkdc-service(使用WebKDC私钥加密)
		此令牌被用于WAS(WebAuth-enabled App Server)与WebKDC之间通讯，它包含一个在WAS与WebKDC之间共享的Session Key
		此令牌仅由WebKDC创建，并仅被WAS使用。从此观点来说，他们是被不透明的发回WebKDC
		此令牌中包含数据：请求的WasCode(s)、SessionKey(k)

	webkdc-proxy(使用WebKDC私钥加密)
		此令牌包含用户代理凭证，仅能由WebKDC解密。WebKDC将仅允许一个webkdc-proxy令牌被用于当初创建它的服务器。webkdc-proxy的主要用途是被WebKDC用来实现单点登录(SSO)；此令牌通常被置于Login组件范围的Cookie中。它的第二个用途是允许WAS请求credential令牌。
		包含数据：WasCode、UserName(s)

	request(使用SessionKey加密)
		此令牌包含WAS从WebKDC请求一个令牌(通常是id令牌)的请求。它采用AES算法使用从webkdc-service获得的Session Key加密，并且包含类似返回地址(return url)和请求令牌类型的信息。一个webkdc-service令牌总是包含于一个request令牌的请求。

	id(使用SessionKey加密)
		此令牌包含试图访问资源的用户的标识。WAS将验证此令牌，并通常会创建一个便于将来使用的app令牌。

	proxy(使用SessionKey加密)
		此令牌用于向WAS返回webkdc-proxy令牌。它包含诸如过期时间和类型的webkdc-proxy的信息。

	cred(使用SessionKey加密)
		此令牌包含用户的凭证。

	login
		EvmAM不使用此令牌。

	app(使用WAS私钥加密)
		此令牌用于WAS服务器存储数据，比如已经从id、proxy或credential令牌验证的用户的标识。


特别注意：
	与WebAuth要求的(网络字节流)不同，本实现中所有数值型数据的均采用x86主机次序存储。

Key-Hint：
	-1：webkdc private key
	-2: was private key
	>=0: session key

A.
A1. WAS向WebKDC请求webkdc-service和session令牌
	请求：
	<getTokensRequest>
     <requesterCredential type="krb5">
        {base64-krb5-mk-req-data}
     </requesterCredential>
     <tokens>
       <token type="service" id="0"/>
     </tokens>
   </getTokensRequest>

   {base64-krb5-mk-req-data}的内容为：base64({hmac}{nonce}{wasCode}{padding})

   返回：
   <getTokensResponse>
    <tokens>
      <token id="0">
        <sessionKey>{base64-session-key}</sessionKey>
        <expires>{expiration-time}</expires>
        <tokenData>{base64}</tokenData><!-- webkdc-service token -->
      </token>
    </tokens>
  </getTokensResponse>