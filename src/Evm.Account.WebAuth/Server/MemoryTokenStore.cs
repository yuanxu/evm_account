﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Caching;
using System.Web;
namespace Evm.AccountManagement.WebAuth.Server
{
    public class MemoryTokenStore:ITokenStore
    {
        /// <summary>
        /// 保存令牌
        /// </summary>
        /// <param name="token"></param>
        /// <param name="expireTime"></param>
        public void Save(string token,int expireTime)
        {
            HttpContext.Current.Cache.Add(token, token, null
                , DateTime.Now.AddMinutes(expireTime)
                , Cache.NoSlidingExpiration
                , CacheItemPriority.Normal
                , null);
        }

        public bool IsValid(string token)
        {
            return HttpContext.Current.Cache.Get(token) != null;
        }
    }
}
