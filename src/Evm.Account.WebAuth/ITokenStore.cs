﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth
{
    /// <summary>
    /// 令牌操纵接口
    /// </summary>
    public interface ITokenStore
    {
        /// <summary>
        /// 保存令牌
        /// </summary>
        /// <param name="token"></param>
        void Save(string token,int expireTime);

        /// <summary>
        /// 判断令牌是否正常
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        bool IsValid(string token);
    }
}
