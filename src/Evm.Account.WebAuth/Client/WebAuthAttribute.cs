﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth.Client
{
    /// <summary>
    /// WebAuth 验证
    /// </summary>
    public class WebAuthAttribute:Attribute
    {
        /// <summary>
        /// 资源名称
        /// </summary>
        public string ResourceCode { get; set; }

        /// <summary>
        /// 权限要求
        /// </summary>
        public uint Permission { get; set; }
    }
}
