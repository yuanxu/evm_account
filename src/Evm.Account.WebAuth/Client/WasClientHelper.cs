﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Web;
using Evm.AccountManagement.WebAuth.Tokens;
using System.Web.Security;
using System.Security.Principal;

namespace Evm.AccountManagement.WebAuth.Client
{

    /// <summary>
    /// Was客户端帮助类
    /// </summary>
    public static class WasClientHelper
    {
        static WasClient instance;

        /// <summary>
        /// 根据Web.Config配置信息初始化
        /// </summary>
        public static void Setup()
        {
            var url = ConfigurationManager.AppSettings["WebKdcUrl"];
           
            Setup(ConfigurationManager.AppSettings["WasCode"]
                ,ConfigurationManager.AppSettings["WasSecret"]
                ,url
                );
        }
        public static void Setup(string wasCode, string secret,string webKdcUrl)
        {
            if (instance == null)
            {
                instance = new WasClient(wasCode, secret);
                WebKdcUrl = webKdcUrl;
                
            }
        }

        /// <summary>
        /// WebKdc地址
        /// </summary>
        public static string WebKdcUrl { get;private set; }

     
        /// <summary>
        /// 确保与WebKdc的链接已经建立
        /// </summary>
        public static void MakeSureWebKdcSessionEstablished()
        {
            if (instance == null)
            {
                throw new ArgumentException("尚未设置初始参数！请首先调用Setup");
            }
            instance.MakeSureWebKdcSessionEstablished(WebKdcUrl);
        }
        public static WasClient Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// 转到WebKdc获取id Token
        /// </summary>
        /// <param name="returnUrl"></param>
        public static void RedirectToGetIdToken(string returnUrl)
        {
            var Server = HttpContext.Current.Server;
            Tokens.RequestToken reqtk = new Tokens.RequestToken(WasClientHelper.Instance.SessionKey);
            reqtk.ReturnUrl = Server.UrlEncode(returnUrl);
            reqtk.RequestedTokenType = "id";

            HttpContext.Current.Response.Redirect(string.Format("{0}?ST={1}&RT={2}", WasClientHelper.WebKdcUrl, Server.UrlEncode(WasClientHelper.Instance.WebKdcServiceToken), Server.UrlEncode(reqtk.Encode())), true);
        }

        /// <summary>
        /// 设置登录状态
        /// </summary>
        public static void SetAuthenticate()
        {
            var Request = HttpContext.Current.Request;
            var Session = HttpContext.Current.Session;
            var Response = HttpContext.Current.Response;

            IdToken it = new IdToken(WasClientHelper.Instance.SessionKey);
            try
            {
                it.DecodeFrom(Request["IT"].Replace(" ", "+"));
            }
            catch (Exception)
            {
                //无法解密，可能是sessionkey已过期。
                //TODO:提示重新登录
                return;
            }
            AppToken at = new AppToken(Encoding.UTF8.GetBytes(WasClientHelper.Instance.Secret));
            at.Acls = it.Acls;
            at.Subject = it.Subject;
            CookieUtils.SetCookie(WebAuthCookieName.webauth_at, at.Encode());
            Session["WebAuth_ACL"] = it.Acls;

            //设置登录状态
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(it.Subject, false, 20);
            FormsIdentity identity = new FormsIdentity(ticket);

            GenericPrincipal principal = new GenericPrincipal(identity, new string[0]);
            HttpContext.Current.User = principal;
            FormsAuthentication.SetAuthCookie(it.Subject, false); //此一句设置登录状态，否则仅仅是这个页面可以得到已登录状态。

            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
            // write to client.
            Response.Cookies.Add(cookie);
        }

        public static void RejectAssess()
        {
            HttpContext.Current.Response.StatusCode = 401;
               HttpContext.Current.Response.SubStatusCode = 3;
               HttpContext.Current.Response.End();
        }
    }
}
