﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using Evm.AccountManagement.WebAuth.Tokens;
using System.Web.Security;
using System.Security.Principal;

namespace Evm.AccountManagement.WebAuth.Client
{
    /// <summary>
    /// WebAuth保护的页面基类。请在应用程序启动时首先调用WasClientHelper.Setup()函数，设置Was参数
    /// </summary>
    public class WebAuthProtectedPage:Page
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!string.IsNullOrEmpty(Request["IT"])) //包含IdToken
            {
                //设置应用自己的AppToken
               WasClientHelper.SetAuthenticate();

            }
            else
            {
                WasClientHelper.MakeSureWebKdcSessionEstablished();

                if (!User.Identity.IsAuthenticated) //转到登录界面
                {
                    WasClientHelper.RedirectToGetIdToken(Request.RawUrl);
                }
            }
        }

       
       
        protected override void OnPreLoad(EventArgs e)
        {
            base.OnPreLoad(e);
            CheckPermissions();
        }

        /// <summary>
        /// 检查权限
        /// </summary>
        protected void CheckPermissions()
        {
            //根据acl和页面的权限设置配置
            var attr = (WebAuthAttribute)Attribute.GetCustomAttribute(GetType(), typeof(WebAuthAttribute));
            if (attr != null)
            {
                if (WebAuthHelper.HasPermission(attr.ResourceCode, attr.Permission))
                {
                    //has permission. nothing to do.
                    return;
                }
                else//not authroized
                {
                    WasClientHelper.RejectAssess();
                }
            }
        }
    }
}
