﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Evm.AccountManagement.WebAuth.Tokens;
using System.IO;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Net;

namespace Evm.AccountManagement.WebAuth.Client
{
    /// <summary>
    /// WebAuth 的WAS客户端实现
    /// </summary>
    [Serializable]
    public class WasClient
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="wasCode">WAS代码</param>
        /// <param name="secret">WAS密码</param>
        public WasClient(string wasCode, string secret)
        {
            WasCode = wasCode;
            Secret = secret;
        }

        /// <summary>
        /// Was代码
        /// </summary>
        public string WasCode { get; private set; }

        /// <summary>
        /// Was密码
        /// </summary>
        public string Secret { get; private set; }

        /// <summary>
        /// SessionKey
        /// </summary>
        public byte[] SessionKey { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string WebKdcServiceToken { get;private set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime WebKdcServiceTokenExpireDate { get; private set; }

        /// <summary>
        /// 确保与WebKdc的链接已经建立
        /// </summary>
        public void MakeSureWebKdcSessionEstablished(string url)
        {
            if (WebKdcServiceToken == null ||WebKdcServiceTokenExpireDate < DateTime.Now )//未建立或已过期
            {
                XmlCommands.GetTokensRequest req = new XmlCommands.GetTokensRequest();
                req.RequesterCredential.Type = "krb5";

                using (MemoryStream stream = new MemoryStream())
                {
                    //hmac的位置
                    stream.Write(new byte[20], 0, 20);

                    stream.Write(Guid.NewGuid().ToByteArray(), 0, 16);
                    var buf = Encoding.UTF8.GetBytes(WasCode);
                    stream.Write(buf, 0, buf.Length);
                    stream.WriteByte((byte)1);
                    

                    var hamc = new HMACSHA1(Encoding.UTF8.GetBytes(Secret));
                    buf = stream.GetBuffer();
                    var computedHamc= hamc.ComputeHash(buf, 20, (int)stream.Length - 20);
                    Debug.Assert(computedHamc.Length == 20);
                    stream.Position = 0;
                    stream.Write(computedHamc, 0, computedHamc.Length);

                    req.RequesterCredential.Token = Convert.ToBase64String(stream.GetBuffer(),0,(int)stream.Length);
                }

                req.Tokens=new XmlCommands.XmlToken[] {new XmlCommands.XmlToken(){  Type="service", Id="0"}};

                var webReq =(HttpWebRequest) HttpWebRequest.Create(url);
                webReq.Method = "POST";
                webReq.ContentType = "text/xml";
                using(StreamWriter writer = new StreamWriter(webReq.GetRequestStream(),Encoding.UTF8))
                {
                    writer.Write(req.GetString());
                }
                var webResp = webReq.GetResponse();
                using (StreamReader reader = new StreamReader(webResp.GetResponseStream()))
                {
                    var getTkResp = XmlCommands.GetTokensResponse.Deserialize<XmlCommands.GetTokensResponse>(reader.ReadToEnd());
                    if (getTkResp.Tokens == null || getTkResp.Tokens.Length == 0) //未拿到正确的token
                    {
                        throw new ArgumentException("未获得正确的token");
                    }
                    SessionKey = Convert.FromBase64String(getTkResp.Tokens[0].SessionKey);
                    this.WebKdcServiceToken = getTkResp.Tokens[0].TokenData;
                    WebKdcServiceTokenExpireDate =getTkResp.Tokens[0].Expires;

                }
            }
        }
    }

}
