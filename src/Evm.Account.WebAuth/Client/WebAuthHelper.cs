﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Evm.AccountManagement.WebAuth.Client
{
   public static class WebAuthHelper
    {
       public static void CheckPermission(string resCode,uint permission)
       {
           if (!string.IsNullOrEmpty(HttpContext.Current.Request["IT"])) //包含IdToken
           {
               //设置应用自己的AppToken
               WasClientHelper.SetAuthenticate();
           }
           if (!IsAuthencated())
           {
               WasClientHelper.MakeSureWebKdcSessionEstablished();
               WasClientHelper.RedirectToGetIdToken(HttpContext.Current.Request.RawUrl);
           }
           if (!HasPermission(resCode, permission))
           {
               WasClientHelper.RejectAssess();
           }
       }
       internal static bool IsAuthencated()
       {
           return HttpContext.Current.Session["WebAuth_ACL"] != null;
       }
       internal static bool HasPermission(string resCode,uint permission)
       {
           var acls=GetAcls();
           return (acls.ContainsKey(resCode)
                   && (acls[resCode] & permission) > 0);
                
       }
       internal static IDictionary<string, uint> GetAcls()
       {
           var acls = (string)HttpContext.Current.Session["WebAuth_ACL"];
           if(string.IsNullOrEmpty(acls))
               return new Dictionary<string, uint>(); 
           var result = new Dictionary<string, uint>();
           foreach (var item in acls.Split(new char[] { ';' }))
           {
               var parts = item.Split(new char[] { ':' });
               
               result.Add(parts[0], uint.Parse(parts[1]));
           }
           return result;
       }
    }
}
