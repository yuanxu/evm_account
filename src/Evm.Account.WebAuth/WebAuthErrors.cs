﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Evm.AccountManagement.WebAuth.XmlCommands;

namespace Evm.AccountManagement.WebAuth
{
    /// <summary>
    /// WebAuth错误
    /// </summary>
    public class WebAuthError
    {
        public WebAuthError() { }

        public WebAuthError(int errorCode,string errorMessage)
        {
            ErrorCdoe = errorCode;
            ErrorMessage = errorMessage;
        }

        /// <summary>
        /// 错误码
        /// </summary>
        public int ErrorCdoe { get; set; }

        /// <summary>
        /// 错误消息
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// 获取错误反馈字符串
        /// </summary>
        /// <returns></returns>
        public string GetErrorResponseString()
        {
            return new ErrorResponse(this).GetString();
        }
    }

    /// <summary>
    /// webkdc-service令牌已过期
    /// </summary>
    public class ServiceTokenExpiredError:WebAuthError
    {
        public ServiceTokenExpiredError() : base(1, "webkdc-service令牌已过期错误") { }
    }

    /// <summary>
    /// webkdc-service令牌不完整或者不能解密
    /// </summary>
    public class ServiceTokenCorruptError:WebAuthError
    {
        public ServiceTokenCorruptError() : base(2, "webkdc-service令牌不完整或者不能解密") { }
    }

    /// <summary>
    /// webkdc-proxy令牌已过期
    /// </summary>
    public class ProxyTokenExpiredError:WebAuthError
    {
        public ProxyTokenExpiredError() : base(3, "webkdc-proxy令牌已过期") { }
    }

    /// <summary>
    /// webkdc-proxy令牌不完整或者不能解密
    /// </summary>
    public class ProxyTokenCorruptError:WebAuthError
    {
        public ProxyTokenCorruptError() : base(4, "webkdc-proxy令牌不完整或者不能解密") { }
    }

    /// <summary>
    /// 请求不完整错误，服务器无法解析XML指令。比如缺少相应的element、attribute等。通常由WAS端产生的错误。
    /// </summary>
    public class RequestInvalidError:WebAuthError
    {
        public RequestInvalidError():base(5,"请求不完整错误，服务器无法解析XML指令。比如缺少相应的element、attribute等。通常由WAS端产生的错误。"){}
    }

    /// <summary>
    /// 请求未授权错误。
    /// </summary>
    public class RequestUnAuthorizedError:WebAuthError
    {
        public RequestUnAuthorizedError() : base(6,"请求未授权错误。") { }
    }

    /// <summary>
    /// 服务器内部错误
    /// </summary>
    public class ServerInternalError:WebAuthError
    {
        public ServerInternalError():base(7,"服务器内部错误"){}
    }

    /// <summary>
    /// request令牌已过时错误。
    /// </summary>
    public class RequestTokenStaleError:WebAuthError
    {
        public RequestTokenStaleError() : base(8, "request令牌已过时错误") { }
    }

    /// <summary>
    /// request令牌不正确
    /// </summary>
    public class RequestTokenInvalidError:WebAuthError
    {
        public RequestTokenInvalidError() : base(9, "request令牌不正确") { }
    }

    /// <summary>
    /// 不能获得请求的cred令牌
    /// </summary>
    public class UnableObtanCredentialTokenError:WebAuthError
    {
        public UnableObtanCredentialTokenError() : base(10, "不能获得请求的cred令牌") { }
    }

    /// <summary>
    /// The krb5 <requesterCredential> was bad.
    /// </summary>
    public class Krb5RequesterCrendentialBasError:WebAuthError
    {
        public Krb5RequesterCrendentialBasError() : base(11, "The krb5 <requesterCredential> was bad.") { }
    }

    /// <summary>
    /// login令牌已过期错误
    /// </summary>
    public class LoginTokenStaleError:WebAuthError
    {
        public LoginTokenStaleError():base(12,"login令牌已过期错误"){}
    }

    /// <summary>
    /// login令牌不正确
    /// </summary>
    public class LoginTokenInvalidError:WebAuthError
    {
        public LoginTokenInvalidError() : base(13, "login令牌不正确") { }
    }

    /// <summary>
    /// 因用户名或密码错误无法登录
    /// </summary>
    public class LoginfailedError:WebAuthError
    {
        public LoginfailedError():base(14,"因用户名或密码错误无法登录"){}
    }

    /// <summary>
    /// 某个需要请求webkdc-proxy令牌的请求中并未包含请求中
    /// </summary>
    public class ProxyTokenNotPresentInRequestError:WebAuthError
    {
        public ProxyTokenNotPresentInRequestError():base(15,"某个需要请求webkdc-proxy令牌的请求中并未包含请求中"){}
    }

    /// <summary>
    /// 登录过程中用户点击了取消
    /// </summary>
    public class UserCanceledError:WebAuthError
    {
        public UserCanceledError() : base(16, "登录过程中用户点击了取消") { }
    }

    /// <summary>
    /// WAS服务器在request令牌中包含了强制登录请求，因此通过webkdc-proxy获取id或者proxy令牌不允许
    /// </summary>
    public class ObtainIdOrProxyTokenNotAllowed:WebAuthError
    {
        public ObtainIdOrProxyTokenNotAllowed() : base(17, "WAS服务器在request令牌中包含了强制登录请求，因此通过webkdc-proxy获取id或者proxy令牌不允许") { }
    }

    /// <summary>
    /// WebKDC不允许用户验证
    /// </summary>
    public class UserNotPermittedAuthencaticationError:WebAuthError
    {
        public UserNotPermittedAuthencaticationError():base(18,"WebKDC不允许用户验证"){}
    }

    /// <summary>
    /// Was凭证错误。可能是无法解析等
    /// </summary>
    public class WasPrincipleCorruptError:WebAuthError
    {
        public WasPrincipleCorruptError() : base(100, "Was凭证错误。可能是无法解析等") { }
    }
}
