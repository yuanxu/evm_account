﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    [XmlRoot("webkdcProxyTokenRequest")]
   public class WebKdcProxyTokenRequest:XmlCommand
    {
        [XmlElement("subjectCredential ")]
        public SubjectCredential SubjectCredential { get; set; }

        [XmlElement("proxyData")]
        public string ProxyData { get; set; }
    }
}
