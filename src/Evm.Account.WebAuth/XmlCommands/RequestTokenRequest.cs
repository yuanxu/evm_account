﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    /// <summary>
    /// Request令牌请求命令
    /// </summary>
    [XmlRoot("requestTokenRequest")]
    [Serializable]
    public class RequestTokenRequest : XmlCommand
    {
        public static RequestTokenRequest Create()
        {
            var obj = new RequestTokenRequest();
            obj.MessageId = "";
            obj.SubjectCredential = new SubjectCredential();
            obj.RequestInfo = new RequestInfo();
            return obj;
        }
        [XmlElement("messageId")]
        public string MessageId { get; set; }

        [XmlElement("subjectCredential")]
        public SubjectCredential SubjectCredential { get; set; }

        [XmlElement("requestToken")]
        public string RequestToken { get; set; }

        [XmlElement("requestInfo")]
        public RequestInfo RequestInfo { get; set; }

       
    }
    
}
