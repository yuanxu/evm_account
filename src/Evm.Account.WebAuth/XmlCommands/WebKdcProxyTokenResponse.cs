﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    [XmlRoot("webkdcProxyTokenResponse")]
   public class WebKdcProxyTokenResponse:XmlCommand
    {
        [XmlElement("webkdcProxyToken")]
        public string WebKdcProxyToken { get; set; }

        [XmlElement("subject")]
        public string Subject { get; set; }
    }
}
