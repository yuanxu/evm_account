﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    [XmlRoot("getTokensResponse")]
    public class GetTokensResponse : XmlCommand
    {
        public GetTokensResponse()
        {
            Tokens = new XmlToken[0];
        }
        [XmlArray("tokens")]
        [XmlArrayItem("token")]
        public XmlToken[] Tokens { get; set; }
    }
}
