﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{

    [Serializable]
    public class RequestInfo
    {
        [XmlElement("remoteUser")]
        public string RemoteUser { get; set; }

        [XmlElement("localIpAddr")]
        public string LocalIpAddr { get; set; }

        [XmlElement("localIpPort")]
        public string LocalIpPort { get; set; }

        [XmlElement("remoteIpAddr")]
        public string RemoteIpAddr { get; set; }

        [XmlElement("remoteIpPort")]
        public string RemoteIpPort { get; set; }
    }
}
