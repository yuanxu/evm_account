﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    [XmlRoot("requestTokenResponse")]
    public class RequestTokenResponse:XmlCommand
    {
        [XmlElement("loginErrorCode")]
        public int LoginErrorCode { get; set; }

        [XmlElement("loginErrorMessage")]
        public string LoginErrorMessage { get; set; }

        [XmlArray("proxyTokens")]
        [XmlArrayItem("proxyToken")]
        public XmlToken[] ProxyTokens { get; set; }

        [XmlElement("returnUrl")]
        public string ReturnUrl { get; set; }

        [XmlElement("requesterSubject")]
        public string RequesterSubject { get; set; }

        [XmlElement("subject")]
        public string Subject { get; set; }

        [XmlElement("RequestedToken")]
        public string requestedToken { get; set; }

        [XmlElement("requestedTokenType")]
        public string RequestedTokenType { get; set; }

        [XmlElement("loginCanceledToken")]
        public string LoginCanceledToken { get; set; }

        [XmlElement("appState")]
        public string AppState { get; set; }
    }
}
