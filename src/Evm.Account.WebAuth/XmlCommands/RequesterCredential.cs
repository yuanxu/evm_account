﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    [Serializable]
    
    public class RequesterCredential
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlText]
        public string Token { get; set; }
    }
}
