﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    [Serializable]
    [XmlRoot("getTokensRequest")]
   public class GetTokensRequest:XmlCommand
    {
        public GetTokensRequest()
        {
            RequesterCredential = new RequesterCredential();
            SubjectCredential = new SubjectCredential();
            Tokens = new XmlToken[0];
        }
        [XmlElement("requesterCredential")]
        public RequesterCredential RequesterCredential { get; set; }


        [XmlElement("subjectCredential")]
        public SubjectCredential SubjectCredential { get; set; }

        [XmlElement("requestToken")]
        public String RequestToken { get; set; }

        [XmlArray("tokens")]
        [XmlArrayItem("token")]
        public XmlToken[] Tokens { get; set; }
    }
}
