﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    [Serializable]
    public class SubjectCredential
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlElement("proxyToken")]
        public string ProxyToken { get; set; }

        [XmlElement("loginToken")]
        public string LoginToken { get; set; }
    }
}
