﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    [Serializable]
    
    public class XmlToken
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlElement("authenticator")]
        public Authenticator Authenticator { get; set; }

        [XmlElement("credentialType")]
        public string CredentialType { get; set; }

        [XmlElement("serverPrincipal")]
        public string ServerPrincipal { get; set; }

        [XmlElement("tokenData")]
        public string TokenData { get; set; }

        [XmlElement("sessionKey")]
        public string SessionKey { get; set; }

        [XmlElement("expires")]
        public DateTime Expires { get; set; }
    }

    
    public class Authenticator
    {
        [XmlAttribute("type")]
        public string Type { get; set; }
    }
}
