﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    [XmlRoot("errorResponse")]
    public class ErrorResponse:XmlCommand
    {
        public ErrorResponse() { }
        public ErrorResponse(WebAuthError error)
        {
            this.ErrorCode = error.ErrorCdoe.ToString();
            this.ErrorMessage = error.ErrorMessage;
        }
        [XmlElement("messageId")]
        public string MessageId { get; set; }

        [XmlElement("errorCode")]
        public string ErrorCode { get; set; }

        [XmlElement("errorMessage")]
        public string ErrorMessage { get; set; }
    }
}
