﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    [XmlRoot("webkdcProxyTokenInfoRequest")]
    class WebKdcProxyTokenInfoRequest:XmlCommand
    {
        [XmlElement("webkdcProxyToken")]
        public string WebKdcProxyToken { get; set; }

    }
}
