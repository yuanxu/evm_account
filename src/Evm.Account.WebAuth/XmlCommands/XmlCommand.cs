﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    /// <summary>
    /// WebAuth XML 协议命令基类
    /// </summary>
   public class XmlCommand
    {
       public string Serialize()
       {
           XmlSerializer x = new XmlSerializer(GetType());
           XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
           //Add an empty namespace and empty value
           ns.Add("", "");
           var builder = new StringBuilder();
           var writer = XmlWriter.Create(builder, new XmlWriterSettings() { OmitXmlDeclaration = true });

           x.Serialize(writer, this, ns);

           return builder.ToString();
       }
       public string GetString()
       {
           return Serialize();
       }
       /// <summary>
       /// 反序列化
       /// </summary>
       /// <typeparam name="T"></typeparam>
       /// <param name="val"></param>
       /// <returns></returns>
       public static T Deserialize<T>(string val)
       {
           XmlSerializer x = new XmlSerializer(typeof(T));
           using (StringReader reader = new StringReader(val))
           {
               return (T)x.Deserialize(reader);
           }
       }

    }
}
