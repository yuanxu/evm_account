﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.AccountManagement.WebAuth.XmlCommands
{
    [XmlRoot("webkdcProxyTokenInfoResponse")]
    public class WebKdcProxyTokenInfoResponse:XmlCommand
    {
        [XmlElement("subject")]
        public string Subject { get; set; }

        [XmlElement("proxyType")]
        public string ProxyType { get; set; }

        [XmlElement("creationTime")]
        public DateTime CreationTime { get; set; }

        [XmlElement("expirationTime")]
        public DateTime ExpirationTime { get; set; }
    }
}
