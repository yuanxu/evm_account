﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Evm.AccountManagement.WebAuth
{
    /// <summary>
    /// Cookie工具
    /// </summary>
    public static class CookieUtils
    {
        public static bool IsExist(WebAuthCookieName name)
        {
            return !string.IsNullOrEmpty(GetCookie(name));
        }

        public static string GetCookie(WebAuthCookieName name)
        {
        var context = HttpContext.Current;
            var cookie = context.Request.Cookies[name.ToString()];
            if (cookie == null)
            {
                //检查url中是否存在
                if (string.IsNullOrEmpty(context.Request.QueryString[name.ToString()]))
                {
                    return "";
                }
                else
                {
                    return context.Request.QueryString[name.ToString()];
                }
            }
            else
            {
                return context.Request.Cookies[name.ToString()].Value;
            }
        }

        public static void SetCookie(WebAuthCookieName name,string value)
        {
            var context = HttpContext.Current;
            context.Request.Cookies.Add(new HttpCookie(name.ToString(), value) );
        }
    }

    public enum WebAuthCookieName
    {
        webauth_wpt_webkdc
        ,webauth_pt_webkdc
        ,webauth_at
    }
}
