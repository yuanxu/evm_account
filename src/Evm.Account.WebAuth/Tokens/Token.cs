﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

namespace Evm.AccountManagement.WebAuth.Tokens
{
    /// <summary>
    /// 令牌抽象类
    /// 所有具体类都应重写PrepareEncode和DecodeFrom方法，并在重写的方法中调用基类的对应方法。
    /// </summary>
    public abstract class Token
    {
        private readonly int KEY_HINT_LEN = 4;
        private readonly int NONCE_LEN = 16;
        private readonly int HMAC_LEN = 20;

        private readonly int AES_BLOCK_SIZE = 16;

        public Token()
        {
            Attributes = new Dictionary<string, byte[]>();
            CreationTime = DateTime.Now;
            ExpirationTime = CreationTime.AddMinutes(5);
        }
        /// <summary>
        /// 分号
        /// </summary>
        private readonly byte SEMICOLON = Convert.ToByte(';');
        private readonly byte EQUAL = Convert.ToByte('=');
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="key">加解密所用的KEY</param>
        public Token(byte[] key):this()
        {
            KeyBytes = key;
        }
        public int KeyHint {
            get
            {
                return KeyHintBytes == null ? 0 : BitConverter.ToInt32(KeyHintBytes, 0);
            }
            set
            {
                KeyHintBytes = BitConverter.GetBytes(value);
            }
        }

        /// <summary>
        /// 4B
        /// </summary>
        protected byte[] KeyHintBytes { get; set; }

        /// <summary>
        /// 16B
        /// </summary>
        protected byte[] NonceBytes { get; set; }

        /// <summary>
        /// 20B
        /// </summary>
        protected byte[] HmacBytes { get; set; }

        protected byte[] AttributeBytes { get; set; }

        protected byte[] PaddingBytes { get; set; }


        public abstract TokenType TokenType { get; }

        /// <summary>
        /// 密钥值
        /// </summary>
        public byte[] KeyBytes { get; set; }

        /// <summary>
        /// 令牌的特性(Attributes)
        /// </summary>
        public IDictionary<string, byte[]> Attributes { get; set; }

        /// <summary>
        /// 令牌的创建时间 
        /// </summary>
        public DateTime CreationTime { get;  set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpirationTime { get;  set; }


        /// <summary>
        /// 编码
        /// 加密公式: base64({key-hint}{nonce}aes({hmac}{token-attributes}{padding}))
        /// {nonce}作为算法的IV使用，并且是不加密的
        /// </summary>
        /// <returns></returns>
        public string Encode()
        {
            PrepareEncode();
            if (KeyHintBytes == null || NonceBytes == null)
                throw new ArgumentException("在未调用PrepareEncode的前提下调用Encode。");
            if (KeyBytes == null || KeyBytes.Length == 0)
                throw new ArgumentException("未设置加密密钥！");
            #region 处理Attributes。结果存于AttributeBytes中

            MemoryStream Buffer = new MemoryStream();

            WriteKV(Buffer, "t", TokenTypeUtil.ToFrendlyString(TokenType));
            if (Attributes != null)
            {
                foreach (var item in Attributes)
                {
                    WriteKV(Buffer, item.Key, item.Value);
                }
            }
            AttributeBytes =new byte[Buffer.Length];
            Array.Copy(Buffer.GetBuffer(), 0, AttributeBytes, 0, AttributeBytes.Length);

            #endregion

            //计算padding
            var padLen = (AttributeBytes.Length + HMAC_LEN) % AES_BLOCK_SIZE;//hnint 不加密
            if (padLen == 0) //如果正好是满足加密所需的padding。根据WebAuth规则，也填充一个多余的Padding，而且长度是16
                padLen = AES_BLOCK_SIZE;
            PaddingBytes = new byte[padLen];
            for (int i = 0; i < padLen; i++)
            {
                PaddingBytes[i] = (byte)padLen;
            }

            //计算hmac
            HmacBytes = ComputeHmac();

            var aes = Aes.Create();

            aes.Key = KeyBytes;
            aes.IV = NonceBytes;
            //aes加密
            var buf = new byte[HmacBytes.Length + AttributeBytes.Length + PaddingBytes.Length];
            using (MemoryStream stream = new MemoryStream(buf))
            {
                stream.Write(HmacBytes, 0, HmacBytes.Length);
                stream.Write(AttributeBytes, 0, AttributeBytes.Length);
                stream.Write(PaddingBytes, 0, PaddingBytes.Length);
            }

            var resultBytes = aes.CreateEncryptor().TransformFinalBlock(buf, 0, buf.Length);

            var resultBuf = new byte[resultBytes.Length + KeyHintBytes.Length + NonceBytes.Length];
            Array.Copy(KeyHintBytes, 0, resultBuf, 0, KeyHintBytes.Length);
            Array.Copy(NonceBytes, 0, resultBuf, KeyHintBytes.Length, NonceBytes.Length);
            Array.Copy(resultBytes, 0, resultBuf, KeyHintBytes.Length + NonceBytes.Length, resultBytes.Length);

            return Convert.ToBase64String(resultBuf);
        }

        private bool isDecode = false;

        /// <summary>
        /// 解码
        /// </summary>
        /// <returns></returns>
        public virtual void DecodeFrom(string value)
        {
            if (KeyBytes == null || KeyBytes.Length == 0)
                throw new ArgumentException("未设置解密密钥！");
            KeyHintBytes = new byte[KEY_HINT_LEN];
            var buf = Convert.FromBase64String(value);
            //KeyHit
            Array.Copy(buf, 0, KeyHintBytes, 0, KEY_HINT_LEN);

            //nonce 
            NonceBytes = new byte[NONCE_LEN];
            Array.Copy(buf, KEY_HINT_LEN, NonceBytes, 0, NONCE_LEN);

            //aes Decode
            var aes = Aes.Create();

            aes.Key = KeyBytes;
            aes.IV = NonceBytes;
            var decodedBuf = aes.CreateDecryptor().TransformFinalBlock(buf, KEY_HINT_LEN + NONCE_LEN, buf.Length - KEY_HINT_LEN - NONCE_LEN);


            //HMAC
            HmacBytes = new byte[HMAC_LEN];
            Array.Copy(decodedBuf, 0, HmacBytes, 0, HMAC_LEN);

            //attribtues
            var padLen = decodedBuf[decodedBuf.Length - 1];
            var len = decodedBuf.Length - padLen - HMAC_LEN;
            AttributeBytes = new byte[len];
            Array.Copy(decodedBuf, HMAC_LEN, AttributeBytes, 0, len);

            //padding
            PaddingBytes = new byte[padLen];
            Array.Copy(decodedBuf, HMAC_LEN + len, PaddingBytes, 0, padLen);

            //解析出一个一个的attribute
            Attributes = new Dictionary<string, byte[]>();
            var offset = 0;
            len = 0;
            for (int i = 0; i < AttributeBytes.Length; i++)
            {
                if (AttributeBytes[i] == SEMICOLON)//检测到分号
                {
                    len = i - offset;
                    //拆分k,v
                    for (int j = offset; j < i; j++)
                    {
                        if (AttributeBytes[j] == EQUAL)
                        {
                            var keyLen = j - offset;
                            var key = Encoding.UTF8.GetString(AttributeBytes, offset, keyLen);
                            var val = new byte[len - keyLen - 1];
                            Array.Copy(AttributeBytes, j + 1, val, 0, val.Length);
                            Attributes.Add(key, val);
                            break;
                        }
                    }
                    offset = i + 1;//;下一位置
                }
            }
            //解析ct,et
            if (Attributes.ContainsKey(TokenAttributes.CreationTime))
            {
                var v = BitConverter.ToUInt32(Attributes[TokenAttributes.CreationTime], 0);

                CreationTime = DateTimeUtils.ToDateTime(v);
            }

            if (Attributes.ContainsKey(TokenAttributes.ExpirationTime))
            {
                var v = BitConverter.ToUInt32(Attributes[TokenAttributes.ExpirationTime], 0);
                ExpirationTime = DateTimeUtils.ToDateTime(v);
            }
            isDecode = true;
        }

        private void WriteKV(MemoryStream Buffer, string key, string value)
        {
            WriteKV(Buffer, key, Encoding.UTF8.GetBytes(value));
        }
        private void WriteKV(MemoryStream Buffer, string key, byte[] value)
        {
            var utf = Encoding.UTF8.GetBytes(key);
            Buffer.Write(utf, 0, utf.Length);
            Buffer.WriteByte(EQUAL);
            Buffer.Write(value, 0, value.Length);
            Buffer.WriteByte(SEMICOLON);
        }

        private byte[] ComputeHmac()
        {
            //计算hmac
            var hmac = HMACSHA1.Create();
            hmac.Key = KeyBytes;
            var buf = new byte[AttributeBytes.Length + PaddingBytes.Length];
            var hmacBytes = hmac.ComputeHash(buf, 0, buf.Length);
            return hmacBytes;
        }



        /// <summary>
        /// 为加密做准备。此函数将检查通用参数是否已经设置，如未设置将自动按照默认规则重置
        /// </summary>
        protected virtual void PrepareEncode()
        {
            if (KeyHint == 0)
            {
                var v = new Random(DateTime.Now.Millisecond).Next(0, Int32.MaxValue);
                KeyHintBytes = BitConverter.GetBytes(v);
            }
            if (NonceBytes == null)
                NonceBytes = Guid.NewGuid().ToByteArray();
            AddTokenAttribute(TokenAttributes.CreationTime, CreationTime);
            AddTokenAttribute(TokenAttributes.ExpirationTime, ExpirationTime);
        }


        /// <summary>
        /// 添加令牌特性信息
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        protected void AddTokenAttribute(string key, string value)
        {
            if (Attributes == null) Attributes = new Dictionary<string, byte[]>();
            if (string.IsNullOrEmpty(value))
                return;
            Attributes.Add(key, Encoding.UTF8.GetBytes(value));

        }

        /// <summary>
        /// 添加令牌特性信息
        /// </summary>
        /// <param name="key"></param>
        /// <param name="dt"></param>
        protected void AddTokenAttribute(string key, DateTime dt)
        {
            if (Attributes == null) Attributes = new Dictionary<string, byte[]>();
            Attributes.Add(key, BitConverter.GetBytes(DateTimeUtils.ToUInt32(dt)));
        }

        /// <summary>
        /// 添加令牌特性信息
        /// </summary>
        /// <param name="key"></param>
        /// <param name="bytes"></param>
        protected void AddTokenAttribute(string key, byte[] bytes)
        {
            if (Attributes == null) Attributes = new Dictionary<string, byte[]>();
            if (bytes == null || bytes.Length == 0)
                return;
            Attributes.Add(key, bytes);
        }

        /// <summary>
        /// 获取令牌特性
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected byte[] GetTokenAttribute(string key)
        {
            if (Attributes == null || !Attributes.ContainsKey(key))
                return new byte[0];
            else
                return Attributes[key];
        }

        /// <summary>
        /// 检验结果是否被篡改过
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            if (!isDecode)
                throw new ArgumentException("尚未解密");
            //重新计算
            var hmac = ComputeHmac();

            for (int i = 0; i < hmac.Length; i++)
            {
                if (hmac[i] != HmacBytes[i])
                    return false;
            }
            return true;
        }

        /// <summary>
        /// 判断是否已过期
        /// </summary>
        /// <returns></returns>
        public bool IsExpired()
        {
            return ExpirationTime < DateTime.Now;
        }
    }


}
