﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth.Tokens
{
    /// <summary>
    /// 
    /// </summary>
    public class ErrorToken:Token
    {
        public ErrorToken(byte[] key) : base(key) { }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public override TokenType TokenType
        {
            get { return Tokens.TokenType.Error; }
        }
        protected override void PrepareEncode()
        {
            base.PrepareEncode();
            AddTokenAttribute(TokenAttributes.ErrorCode, ErrorCode);
            AddTokenAttribute(TokenAttributes.ErrorMessage, ErrorMessage);
        }
        public override void DecodeFrom(string value)
        {
            base.DecodeFrom(value);
            ErrorCode = GetTokenAttribute(TokenAttributes.ErrorCode).GetString();
            ErrorMessage = GetTokenAttribute(TokenAttributes.ErrorMessage).GetString();
        }
    }
}
