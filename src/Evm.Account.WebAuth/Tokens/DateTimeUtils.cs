﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth.Tokens
{
    /// <summary>
    /// 日期函数
    /// </summary>
    public static class DateTimeUtils
    {
        private static readonly DateTime DT1970 = new DateTime(1970, 1, 1, 0, 0, 0);

        /// <summary>
        /// 转换为自UTC 1970-1-1 0:0:0以来的秒数
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static UInt32 ToUInt32(DateTime dt)
        {
            var val = dt;
            if (dt.Kind == DateTimeKind.Local || dt.Kind == DateTimeKind.Unspecified)
            {
                val = dt.ToUniversalTime();
            }
            var ts = dt.Subtract(DT1970);
            return (UInt32)ts.TotalSeconds;
        }

        /// <summary>
        /// 转换为UTC时间
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static DateTime ToUtcDateTime(UInt32 v)
        {
           return  DT1970.AddSeconds(v);
        }

        /// <summary>
        /// 转换为本地时间
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(UInt32 v)
        {
            var dt = ToUtcDateTime(v);
            return dt.ToLocalTime();
        }
    }
}
