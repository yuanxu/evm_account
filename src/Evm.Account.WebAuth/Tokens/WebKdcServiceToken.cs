﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth.Tokens
{
    /// <summary>
    /// webkdc-service token
    /// </summary>
   public  class WebKdcServiceToken:Token
    {
       public WebKdcServiceToken(byte[] key) : base(key) { }
       /// <summary>
       /// Session key
       /// </summary>
       public byte[] SessionKey { get; set; }
       public void GenerateSessionKey()
       {
           SessionKey = Guid.NewGuid().ToByteArray();
       }
       
       public override TokenType TokenType
       {
           get { return WebAuth.Tokens.TokenType.WebKdcService; }
       }
       protected override void PrepareEncode()
       {
           base.PrepareEncode();
           this.AddTokenAttribute(TokenAttributes.Subject, Subject);
           AddTokenAttribute(TokenAttributes.SessionKey, SessionKey);
       }
       public override void DecodeFrom(string value)
       {
           base.DecodeFrom(value);
           Subject = Attributes[TokenAttributes.Subject].GetString();
           SessionKey = Attributes[TokenAttributes.SessionKey];
       }

       public string Subject { get; set; }
    }
}
