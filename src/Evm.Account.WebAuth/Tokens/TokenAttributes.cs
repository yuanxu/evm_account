﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth.Tokens
{
    sealed class TokenAttributes
    {
        /// <summary>
        /// request令牌的可选数据。如果提供了，此参数将作为Return Url的第二个参数返回给WAS，不包含在返回的加密数据中.
        /// Optional data included in the request token. If present, the same data will be sent back to the WAS as a second parameter to the URL, not included in the encrypted response token.
        /// </summary>
        public const string As = "as";

        /// <summary>
        /// 正在执行的XML 命令名称。包含于request令牌
        /// The name of the XML command being executed (for example, getTokensRequest). Included in the request token sent for XML commands.
        /// </summary>
        public const string Cmd = "cmd";

        /// <summary>
        /// 凭证数据
        /// Credential data (for example, an encoded Kerberos V5 service ticket).
        /// </summary>
        public const string CredentialData = "crd";

        /// <summary>
        /// 用于访问的服务凭证数据标识
        /// The identity of the service credential data can be used to access (normally the text representation of the server principal for a Kerberos V5 service ticket).
        /// </summary>
        public const string CredentialService = "crs";

        /// <summary>
        /// 凭证类型.当前总是krb5
        /// Credential type. Currently this is always krb5.
        /// </summary>
        public const string CredentialType = "crt";

        /// <summary>
        /// 凭证的创建时间。用于确保凭证的时效性
        /// Creation time of the token. For tokens used to exchange messages between servers (request, error, id, proxy, credential), this value is used to ensure that the request is fresh. For example, tokens of this type with a ct older then 5 minutes will get flagged by the server (WAS or WebKDC) as being stale.
        /// </summary>
        public const string CreationTime = "ct";

        /// <summary>
        /// 从WebKDC返回的错误码
        /// Error code from the WebKDC. This will be the ASCII digit representation of one of the error codes that are returned in XML messages.
        /// </summary>
        public const string ErrorCode = "ec";

        /// <summary>
        /// 从WebKDC返回的错误描述
        /// Error message from the WebKDC. This should only be used for logging and/or debugging, since it is not localized and not in a format meant for end-user consumption.
        /// </summary>
        public const string ErrorMessage = "em";

        /// <summary>
        /// 令牌的过期时间
        /// Expiration time of the token.
        /// </summary>
        public const string ExpirationTime = "et";

        /// <summary>
        /// WebKDC Session Key
        /// </summary>
        public const string SessionKey = "k";

        /// <summary>
        ///最后使用时间。如果app令牌中包含了此特性，WAS应该周期性的更新此值。此特性被用于app令牌未过期时的超时未用功能。 
        ///Last-used time. If this attribute is set in an app token present in a cookie, the WAS SHOULD periodically update it (by setting a new cookie with an updated last-used time) as it is used for access. This attribute is used to implement timing out of unused but still unexpired app tokens.
        /// </summary>
        public const string LastUsedTime = "lt";

        /// <summary>
        /// 用户密码
        /// User's password, in a login token.
        /// </summary>
        public const string UserPassword = "p";

        /// <summary>
        /// 代理数据
        /// Proxy data, such as an encoded Kerberos V5 TgT.
        /// </summary>
        public const string ProxyData = "pd";

        /// <summary>
        /// 代理主题。webkdc-service令牌的主题当webkdc-proxy令牌创建时被使用。被用于验证webkdc-proxy令牌正在被创建他的统一实体所使用。
        /// </summary>
        public const string ProxySubject = "ps";

        /// <summary>
        /// 代理类型。比如krb5
        /// Proxy type (such as krb5).
        /// </summary>
        public const string ProxyType = "pt";

        /// <summary>
        /// 请求选项。当前以下选项是被支持的：lc,如果登录被取消向应用返回错误吗；fa,如果用户具有一个webkdc-proxy令牌强制执行交互式验证
        /// Comma-separated list of request options. Currently, the following options are supported: lc, to return an error code to the application if login is cancelled; and fa, to force interactive authentication even if the user has a webkdc-proxy token.
        /// </summary>
        public const string RequestOption = "ro";

        /// <summary>
        /// request令牌中已请求的令牌类型。或者是id用于一个id令牌或者是proxy用于一个proxy令牌
        /// Requested token type in a request token. This is either id for an id token or proxy for a proxy token.
        /// </summary>
        public const string RequestedTokenType = "rtt";

        /// <summary>
        /// 返回地址的URL。
        /// Return URL. The user to return the user to after authentication.
        /// </summary>
        public const string ReturnUrl = "ru";

        /// <summary>
        /// 验证主题。在webkdc-service令牌中，此值用来记录请求获取令牌的服务器名称。其他情况，是已经验证的用户。目前服务器主题(server subjects)的格式是 "type:identifier"。当前仅有krb5被定义
        /// The authenticated subject. In webkdc-service tokens, this is the server identity that authenticated to get the webkdc-service token. In all other tokens, it is the user who authenticated. Server subjects have the form "type:identifier". Currently, the only defined type is "krb5" indicating that the server authenticated with Kerberos V5, and in that case the identifier is the text form of a fully qualified Kerberos V5 principal.
        /// </summary>
        public const string Subject = "s";

        /// <summary>
        /// id令牌中的主题验证类别。可以是krb5或者webkdc。本实现中取固定值webkdc
        /// Subject authenticator type in an id token. This is currently either krb5, indicating that a Kerberos V5 authenticator is included, or webkdc, indicating no additional authenticator is provided.
        /// </summary>
        public const string SubjectAuthenticatorType = "sa";

        /// <summary>
        /// 主题验证数据
        /// Subject authenticator data. If the sa is krb5, this is a KRB_AP_REQ for the same Kerberos V5 principal as the webkdc-service token's subject.
        /// </summary>
        public const string SubjectAuthenticatorData = "sad";

        /// <summary>
        /// 令牌类型
        /// 当前被识别的类别包括：webkdc-service, webkdc-proxy, req, error, id, proxy, cred, and app. 服务器用此确保令牌被用于正确的目的。
        /// The token type. Currently recognized token types are webkdc-service, webkdc-proxy, req, error, id, proxy, cred, and app. Used by a server to ensure that a token is being used for the correct purpose.
        /// </summary>
        public const string TokenType = "t";

        /// <summary>
        /// 被包含于其他令牌中的webkdc-service或webkdc-proxy
        /// A webkdc-proxy or webkdc-service token that is being included inside another token.
        /// </summary>
        public const string WebKdcTokenType = "wt";

        /// <summary>
        /// login令牌中的用户名
        /// The user's username in a login token.
        /// </summary>
        public const string UserName = "u";

        /// <summary>
        /// 访问控制列表。EvmAM扩展特性。
        /// 格式为：ResourceCode:PermissionValue[;ResourceCode:PermissionValue...]
        /// </summary>
        public const string Acls = "acl";

        /// <summary>
        /// 所属角色列表。EvmAM扩展特性
        /// 格式为：RoleName@OrganCode:isConcurrency[;RoleName:isConcurrency]
        /// </summary>
        public const string Roles = "role";
    }
}
