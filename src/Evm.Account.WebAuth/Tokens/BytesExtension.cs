﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth.Tokens
{
   public static class BytesExtension
    {
       /// <summary>
       /// 使用UTF8解码
       /// </summary>
       /// <param name="val"></param>
       /// <returns></returns>
       public static string GetString(this byte[] val)
       {
           if (val == null || val.Length == 0)
               return "";
           return Encoding.UTF8.GetString(val);
       }
    }
}
