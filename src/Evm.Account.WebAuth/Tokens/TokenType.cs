﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth.Tokens
{
    /// <summary>
    /// 令牌类型
    /// </summary>
    public enum TokenType
    {
        /// <summary>
        /// 此令牌被用于WAS(WebAuth-enabled App Server)与WebKDC之间通讯，它包含一个在WAS与WebKDC之间共享的Session Key
		/// 此令牌仅由WebKDC创建，并仅被WAS使用。从此观点来说，他们是被不同命的发回WebKDC
        /// </summary>
        WebKdcService,

        /// <summary>
        /// 此令牌包含用户代理凭证，仅能由WebKDC解密。WebKDC将仅允许一个webkdc-proxy令牌被用于当初创建它的服务器。webkdc-proxy的主要用途是被WebKDC用来实现单点登录(SSO)；此令牌通常被置于Login组件范围的Cookie中。它的第二个用途是允许WAS请求credential令牌。
        /// </summary>
        WebKdcProxy,

        /// <summary>
        ///	此令牌包含WAS从WebKDC请求一个令牌(通常是id令牌)的请求。它采用AES算法使用从webkdc-service获得的Session Key加密，并且包含类似返回地址(return url)和请求令牌类型的信息。一个webkdc-service令牌总是包含于一个request令牌的请求。
        /// </summary>
        Request,

        /// <summary>
        /// 
        /// </summary>
        Error,

        /// <summary>
        /// 此令牌包含试图访问资源的用户的标识。WAS将验证此令牌，并通常会创建一个便于将来使用的app令牌。
        /// </summary>
        Id,

        /// <summary>
        /// 此令牌用于向WAS返回webkdc-proxy令牌。它包含诸如过期时间和类型的webkdc-proxy的信息。
        /// </summary>
        Proxy,

        /// <summary>
        /// 此令牌包含用户的凭证
        /// </summary>
        Cred,

        /// <summary>
        /// 登录令牌。用于Login 组件向Kdc请求登录
        /// </summary>
        Login,

        /// <summary>
        /// 此令牌用于WAS服务器存储数据，比如已经从id、proxy或credential令牌验证的用户的标识。
        /// </summary>
        App
    }

    static class TokenTypeUtil
    {
        public static string ToFrendlyString(this TokenType t)
        {
            switch (t)
            {
                case TokenType.WebKdcService:
                    return "webkdc-service";
                case TokenType.WebKdcProxy:
                    return "webkdc-proxy";
                case TokenType.Request:
                    return "req";
                case TokenType.Error:
                    return "error";
                case TokenType.Id:
                    return "id";
                case TokenType.Proxy:
                    return "proxy";
                case TokenType.Cred:
                    return "cred";
                case TokenType.Login:
                    return "login";
                case TokenType.App:
                    return "app";
                default:
                    return "unknown";
            }
        }
    }
}
