﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth.Tokens
{
    /// <summary>
    /// 此令牌包含试图访问资源的用户的标识。WAS将验证此令牌，并通常会创建一个便于将来使用的app令牌。
    /// </summary>
    public class IdToken:Token
    {
        public IdToken(byte[] key) : base(key) { }
        public override TokenType TokenType
        {
            get { return Tokens.TokenType.Id; }
        }

        /// <summary>
        ///  id令牌中的主题验证类别。可以是krb5或者webkdc。本实现中取固定值webkdc
        /// </summary>
        public string SubjectAuthenticatorType { get; set; }

        /// <summary>
        /// /验证主题。在webkdc-service令牌中，此值用来记录请求获取令牌的服务器名称。其他情况，是已经验证的用户。目前服务器主题(server subjects)的格式是 "type:identifier"。当前仅有krb5被定义
        /// </summary>
        public string Subject { get; set; }

        private string acls = "";
        /// <summary>
        /// 访问控制列表字符串。格式：资源代码:权限[;资源代码:权限]
        /// </summary>
        public string Acls
        {
            get { return acls; }
            set { acls = value; aclDict = null; }
        }

        private IDictionary<string, uint> aclDict;
        /// <summary>
        /// 访问控制列表字典
        /// </summary>
        public IDictionary<string, uint> AclDictionary
        {
            get
            {
                if (aclDict == null)
                    aclDict = new Dictionary<string, uint>();
                if (Acls.Length > 0)
                {
                    foreach (var item in Acls.Split(new char[]{';'}))
                    {
                        var parts = item.Split(new char[]{':'});
                        aclDict.Add(parts[0], uint.Parse(parts[1]));
                    }
                }
                return aclDict;
            }
        }

        private string roles = "";
        /// <summary>
        /// 所属角色字符串。格式：角色名@OrganCode:是否兼职[;角色名@OrganCode:是否兼职]
        /// </summary>
        public string Roles
        {
            get
            {
                return roles;
            }
            set
            {
                roles = value;
                
            }
        }
        private IDictionary<string, bool> roleDict = null;

        /// <summary>
        /// 角色访问控制列表
        /// </summary>
        public IDictionary<string,bool> RoleDictionary
        {
            get
            {
                if (roleDict == null)
                    roleDict = new Dictionary<string, bool>();
                if (Roles.Length > 0)
                {
                    foreach (var item in Roles.Split(new char[] { ';' }))
                    {
                        var parts = item.Split(new char[] { ':' });
                        roleDict.Add(parts[0], bool.Parse(parts[1]));
                    }
                }
                return roleDict;
            }
        }

        protected override void PrepareEncode()
        {
            base.PrepareEncode();
            AddTokenAttribute(TokenAttributes.SubjectAuthenticatorType, SubjectAuthenticatorType);
            AddTokenAttribute(TokenAttributes.Subject, Subject);
            AddTokenAttribute(TokenAttributes.Acls, Acls);
            AddTokenAttribute(TokenAttributes.Roles, Roles);
        }

        public override void DecodeFrom(string value)
        {
            base.DecodeFrom(value);
            SubjectAuthenticatorType = GetTokenAttribute(TokenAttributes.SubjectAuthenticatorType).GetString();
            Subject = GetTokenAttribute(TokenAttributes.Subject).GetString();
            Acls= GetTokenAttribute(TokenAttributes.Acls).GetString();
            Roles = GetTokenAttribute(TokenAttributes.Roles).GetString();
        }
    }
}
