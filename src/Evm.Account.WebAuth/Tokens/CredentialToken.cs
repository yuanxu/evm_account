﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth.Tokens
{
    /// <summary>
    /// 此令牌包含用户的凭证
    /// </summary>
    public class CredentialToken:Token
    {
        public CredentialToken(byte[] key) : base(key) { }
        public override TokenType TokenType
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// 用于访问的服务凭证数据标识
        /// </summary>
        public string CredentialService { get; set; }

        /// <summary>
        /// 凭证类型.当前总是krb5
        /// </summary>
        public string CredentialType { get; set; }

        /// <summary>
        /// 验证主题。在webkdc-service令牌中，此值用来记录请求获取令牌的服务器名称。其他情况，是已经验证的用户。目前服务器主题(server subjects)的格式是 "type:identifier"。当前仅有krb5被定义
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// 凭证数据
        /// </summary>
        public byte[] CredentialData { get; set; }

        protected override void PrepareEncode()
        {
            base.PrepareEncode();
            AddTokenAttribute(TokenAttributes.CredentialService, CredentialService);
            AddTokenAttribute(TokenAttributes.CredentialType, CredentialType);
            AddTokenAttribute(TokenAttributes.Subject, Subject);
            AddTokenAttribute(TokenAttributes.CredentialData, CredentialData);
        }
        public override void DecodeFrom(string value)
        {
            base.DecodeFrom(value);
            CredentialService = GetTokenAttribute(TokenAttributes.CredentialService).GetString();
            CredentialType = GetTokenAttribute(TokenAttributes.CredentialType).GetString();
            Subject = GetTokenAttribute(TokenAttributes.Subject).GetString();
            CredentialData = GetTokenAttribute(TokenAttributes.CredentialData);
        }
    }
}
