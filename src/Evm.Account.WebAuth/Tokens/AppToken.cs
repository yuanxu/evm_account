﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth.Tokens
{
    /// <summary>
    /// 此令牌用于WAS服务器存储数据，比如已经从id、proxy或credential令牌验证的用户的标识。
    /// </summary>
   public  class AppToken:Token
    {
       public AppToken(byte[] key) : base(key) { }

        public override TokenType TokenType
        {
            get { return Tokens.TokenType.App; }
        }

       /// <summary>
        /// 验证主题。在webkdc-service令牌中，此值用来记录请求获取令牌的服务器名称。其他情况，是已经验证的用户。目前服务器主题(server subjects)的格式是 "type:identifier"。当前仅有krb5被定义
       /// </summary>
        public string Subject { get; set; }

       /// <summary>
        ///{session-key} is only present when an app token is being used as the application state inside of a request token. This is done when serving the same web resource with a pool of servers that all should be able to decrypt the id token from the WebKDC.
       /// </summary>
        public byte[] SessionKey { get; set; }

       /// <summary>
        /// 最后使用时间。如果app令牌中包含了此特性，WAS应该周期性的更新此值。此特性被用于app令牌未过期时的超时未用功能。 
       /// </summary>
        public DateTime LastUseTime { get; set; }

       /// <summary>
       /// 访问控制列表
       /// </summary>
        public string Acls { get; set; }

        protected override void PrepareEncode()
        {
            base.PrepareEncode();
            AddTokenAttribute(TokenAttributes.Subject, Subject);
            AddTokenAttribute(TokenAttributes.SessionKey, SessionKey);
            AddTokenAttribute(TokenAttributes.LastUsedTime, LastUseTime);
            AddTokenAttribute(TokenAttributes.Acls, Acls);
        }

        public override void DecodeFrom(string value)
        {
            base.DecodeFrom(value);
            Subject = GetTokenAttribute(TokenAttributes.Subject).GetString();
            SessionKey = GetTokenAttribute(TokenAttributes.SessionKey);
            var v = GetTokenAttribute(TokenAttributes.LastUsedTime).GetString();
            if (!string.IsNullOrEmpty(v))
                LastUseTime = DateTimeUtils.ToDateTime(Convert.ToUInt32(v));
            Acls = GetTokenAttribute(TokenAttributes.Acls).GetString();
           
        }
    }
}
