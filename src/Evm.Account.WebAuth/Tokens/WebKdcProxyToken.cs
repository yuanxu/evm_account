﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth.Tokens
{
   public  class WebKdcProxyToken:Token,ICloneable
    {
       public WebKdcProxyToken(byte[] key) : base(key) { }
       /// <summary>
       /// 代理主题。webkdc-service令牌的主题当webkdc-proxy令牌创建时被使用。被用于验证webkdc-proxy令牌正在被创建他的统一实体所使用.
       /// </summary>
       public string ProxySubject { get; set; }

       /// <summary>
       /// 验证主题。在webkdc-service令牌中，此值用来记录请求获取令牌的服务器名称。其他情况，是已经验证的用户。目前服务器主题(server subjects)的格式是 "type:identifier"。当前仅有krb5被定义
       /// The authenticated subject. In webkdc-service tokens, this is the server identity that authenticated to get the webkdc-service token. In all other tokens, it is the user who authenticated. Server subjects have the form "type:identifier". Currently, the only defined type is "krb5" indicating that the server authenticated with Kerberos V5, and in that case the identifier is the text form of a fully qualified Kerberos V5 principal.
       /// </summary>
       public string Subject { get; set; }

       /// <summary>
       /// 代理类型。比如krb5
       /// Proxy type (such as krb5).
       /// </summary>
       public string ProxyType { get; set; }

       /// <summary>
       /// 代理数据
       /// Proxy data, such as an encoded Kerberos V5 TgT.
       /// </summary>
       public byte[] ProxyData { get; set; }

        public override TokenType TokenType
        {
            get { return Tokens.TokenType.WebKdcProxy; }
        }
        protected override void PrepareEncode()
        {
            base.PrepareEncode();
            AddTokenAttribute(TokenAttributes.ProxySubject, ProxySubject);
            AddTokenAttribute(TokenAttributes.ProxyType, ProxyType);
            AddTokenAttribute(TokenAttributes.Subject, Subject);
            AddTokenAttribute(TokenAttributes.ProxyData, ProxyData);
        }
        public override void DecodeFrom(string value)
        {
            base.DecodeFrom(value);
            ProxySubject = GetTokenAttribute(TokenAttributes.ProxySubject).GetString();
            ProxyType = GetTokenAttribute(TokenAttributes.ProxyType).GetString();
            Subject = GetTokenAttribute(TokenAttributes.Subject).GetString();
        }



        public object Clone()
        {
            var tk= (WebKdcProxyToken)this.MemberwiseClone();
            tk.CreationTime = DateTime.Now;
            return tk;
        }
    }
}
