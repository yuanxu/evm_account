﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.AccountManagement.WebAuth.Tokens
{
    /// <summary>
    /// 		此令牌包含WAS从WebKDC请求一个令牌(通常是id令牌)的请求。它采用AES算法使用从webkdc-service获得的Session Key加密，并且包含类似返回地址(return url)和请求令牌类型的信息。一个webkdc-service令牌总是包含于一个request令牌的请求。
    /// </summary>
    public class RequestToken:Token
    {
        public RequestToken(byte[] key):base(key)
        {}

        public override TokenType TokenType
        {
            get { return Tokens.TokenType.Request; }
        }

        /// <summary>
        /// 返回地址的URL。
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// request令牌中已请求的令牌类型。或者是id用于一个id令牌或者是proxy用于一个proxy令牌
        /// </summary>
        public string RequestedTokenType { get; set; }

        /// <summary>
        /// request令牌的可选数据。如果提供了，此参数将作为Return Url的第二个参数返回给WAS，不包含在返回的加密数据中
        /// </summary>
        public string As { get; set; }

        /// <summary>
        /// id令牌中的主题验证类别。可以是krb5或者webkdc。本实现中取固定值webkdc
        /// </summary>
        public string SubjectAuthenticatorType { get; set; }

        /// <summary>
        /// 代理类型。比如krb5
        /// </summary>
        public string ProxyType { get; set; }

        /// <summary>
        /// 正在执行的XML 命令名称。包含于request令牌
        /// </summary>
        public string CommandToExecute { get; set; }

        protected override void PrepareEncode()
        {
            base.PrepareEncode();
            AddTokenAttribute(TokenAttributes.ReturnUrl, ReturnUrl);
            AddTokenAttribute(TokenAttributes.RequestedTokenType, RequestedTokenType);
            AddTokenAttribute(TokenAttributes.As, As);
            AddTokenAttribute(TokenAttributes.SubjectAuthenticatorType, SubjectAuthenticatorType);
            AddTokenAttribute(TokenAttributes.ProxyType, ProxyType);
            AddTokenAttribute(TokenAttributes.Cmd, CommandToExecute);
        }
        public override void DecodeFrom(string value)
        {
            base.DecodeFrom(value);
            ReturnUrl = GetTokenAttribute(TokenAttributes.ReturnUrl).GetString();
            RequestedTokenType = GetTokenAttribute(TokenAttributes.RequestedTokenType).GetString();
            As = GetTokenAttribute(TokenAttributes.As).GetString();
            SubjectAuthenticatorType = GetTokenAttribute(TokenAttributes.SubjectAuthenticatorType).GetString();
            ProxyType = GetTokenAttribute(TokenAttributes.ProxyType).GetString();
            CommandToExecute = GetTokenAttribute(TokenAttributes.Cmd).GetString();
        }
    }
}
